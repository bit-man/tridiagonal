/*
 * FamiliaTiendeACero3.h
 *
 *  Created on: 04/04/2009
 *      Author: claudio
 */

#ifndef FAMILIATIENDEACERO3_H_
#define FAMILIATIENDEACERO3_H_

#include "FamiliaTriangulos.h"

class FamiliaTiendeACero3: public FamiliaTriangulos {
	list<Triangulo> familia;
	static const int cantidadMaxima = 700;
public:
	list<Triangulo> getFamilia(unsigned int precision);
    string getNombre();
};

#endif /* FAMILIATIENDEACERO3_H_ */
