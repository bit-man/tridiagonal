
#ifndef FAMILIATRIANGULOS_H_
#define FAMILIATRIANGULOS_H_

#include<list>
#include "Triangulo.h"

class FamiliaTriangulos {
	list<Triangulo> familia;
public:
	virtual list<Triangulo> getFamilia(unsigned int precision);
    virtual string getNombre();
};

#endif /* FAMILIATRIANGULOS_H_ */
