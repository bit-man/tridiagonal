
#include "FamiliaTiendeACero3.h"
#include <cmath>

list<Triangulo> FamiliaTiendeACero3::getFamilia(unsigned int precision) {
    familia.clear();
	PrecisionVariable lado1(precision),lado2(precision),area(64),altura(64),unCuarto(64);
	lado1 = (long double)1;
	unCuarto = (long double)1 / (long double)4;
	for(int i = 1; i <= cantidadMaxima; i++) {
		altura = lado1.getReal() / (long double)pow((long double)i,(long double)4);
		lado2 = raiz(unCuarto + (altura * altura));
		area = altura / (long double) 2;
		Triangulo *t = new Triangulo(precision,lado1,lado2,lado2,area);
		familia.push_back(*t);
	}
	return familia;
}

string FamiliaTiendeACero3::getNombre() {
    return "FamiliaTiendeACero3";
}
