
#ifndef FAMILIATIENDEACERO2_H_
#define FAMILIATIENDEACERO2_H_

#include "FamiliaTriangulos.h"
#include<cmath>

class FamiliaTiendeACero2: public FamiliaTriangulos {
	list<Triangulo> familia;
	static const int cantidadMaxima = 500;
public:
	list<Triangulo> getFamilia(unsigned int);
    string getNombre();
};

#endif /* FAMILIATIENDEACERO2_H_ */
