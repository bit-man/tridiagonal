
#ifndef FAMILIAPIOLIN_H_
#define FAMILIAPIOLIN_H_

#include "FamiliaTriangulos.h"

#define BIG_NUMBER      long double
#define MAX_PRECISION   64

class FamiliaPiolin: public FamiliaTriangulos {
	list<Triangulo> familia;
public:
	list<Triangulo> getFamilia(unsigned int precision);
    string getNombre();
};


#endif /* FAMILIAPIOLIN_H_ */

