
#ifndef FAMILIATIENDEACERO1_H_
#define FAMILIATIENDEACERO1_H_

#include "FamiliaTriangulos.h"
#include<cmath>

class FamiliaTiendeACero1 : public FamiliaTriangulos {
	list<Triangulo> familia;
	static const int cantidadMaxima = 4500;
public:
	list<Triangulo> getFamilia(unsigned int);
    string getNombre();
};

#endif /* FAMILIATIENDEACERO1_H_ */
