#include "FamiliaPiolin.h"
#include <cmath>

/**
 * Parece tonto poner la superficie si se puede calcular teniendo base y altura,
 * pero la idea es que esta sea un número que pueda almacenarse sin errores
 * (por ejemplo que sea potencia de 2) y al tener que definirlo y que sea igua
 * a base por altura sobre 2 enfatiza este punto (evita que la superficie sea
 * un número que está sujeto a redondeo o truncamiento y produzca errores)
 **/

#define SUPERFICIE      (BIG_NUMBER) 4.0
#define BASE            (BIG_NUMBER) 160.0
#define ALTURA          (BIG_NUMBER) 0.05
#define ALTURA_AL_CUAD  (BIG_NUMBER) pow(ALTURA, (BIG_NUMBER) 2)


#define NUM_TRIANG_IZQ         200
#define NUM_TRIANG_CASITA      100
#define DELTA                  (3 * BASE / (NUM_TRIANG_CASITA + NUM_TRIANG_IZQ))


list<Triangulo> FamiliaPiolin::getFamilia(unsigned int precision) {
	PrecisionVariable b1(precision), b2(precision), l1(precision), l2(precision),
                      base(precision), sup(MAX_PRECISION);
    familia.clear();

    /**
     * Cálculo de los lados del triángulo alargado a la izquierda
     **/
    b2 = BASE;
    sup = SUPERFICIE;

    for( int nTriang = NUM_TRIANG_IZQ; nTriang > 0; nTriang--) {
		//Se podría haber hecho b1 += DELTA y b2 -= DELTA pero eso
		// acumula errores en cada iteración y puede pasar que
		// la última iteración no coincida b1 con BASE (sea un poco
		// menor debido a la propagación de de errores)
		b1 = (BIG_NUMBER) nTriang * DELTA;
		base = b2 + b1;

		l1 = (BIG_NUMBER) sqrt( pow(b1.getReal(), (BIG_NUMBER) 2) + ALTURA_AL_CUAD );
		l2 = (BIG_NUMBER) sqrt( pow(base.getReal(), (BIG_NUMBER) 2) + ALTURA_AL_CUAD );
		Triangulo *t = new Triangulo(precision,l1,l2,b2,sup);
		familia.push_back(*t);
	}

    
    /**
     * Cálculo de los lados del triángulo en forma de casita :-D
     **/
    base = BASE;
    sup = SUPERFICIE;
    
	for( int nTriang = 0; nTriang <= NUM_TRIANG_CASITA; nTriang++) {
		//Se podría haber hecho b1 += DELTA y b2 -= DELTA pero eso
		// acumula errores en cada iteración y puede pasar que
		// la última iteración no coincida b1 con BASE (sea un poco
		// menor debido a la propagación de de errores)
		b1 = (BIG_NUMBER) nTriang * DELTA;
		b2 = base - b1;

		l1 = (BIG_NUMBER) sqrt( pow(b1.getReal(), (BIG_NUMBER) 2) + ALTURA_AL_CUAD );
		l2 = (BIG_NUMBER) sqrt( pow(b2.getReal(), (BIG_NUMBER) 2) + ALTURA_AL_CUAD );
		Triangulo *t = new Triangulo(precision,l1,l2,base,sup);
		familia.push_back(*t);
	}

	return familia;
}

string FamiliaPiolin::getNombre() {
    return "FamiliaPiolin";
}

