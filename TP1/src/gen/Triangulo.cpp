#include "Triangulo.h"
#include <cmath>
#include <string>

Triangulo::Triangulo(unsigned int precision, PrecisionVariable lado1, PrecisionVariable lado2, PrecisionVariable lado3, PrecisionVariable area)
:lado1(precision),lado2(precision),lado3(precision),area(precision) {

	this->lado1 = lado1;
	this->lado2 = lado2;
	this->lado3 = lado3;
	this->area = area;
	this->precision = precision;
}

Triangulo::~Triangulo() {

}

PrecisionVariable& Triangulo::getLado1() {
	return lado1;
}

PrecisionVariable& Triangulo::getLado2() {
	return lado2;
}

PrecisionVariable& Triangulo::getLado3() {
	return lado3;
}

PrecisionVariable& Triangulo::getArea() {
	return area;
}

void Triangulo::_showHeaderError( string preStr, PrecisionVariable& lado1,
                                  PrecisionVariable& lado2, PrecisionVariable& lado3) {

        cout << preStr << ": l1=" << lado1 << "  l2=" << lado2 << "  l3=" << lado3 << endl;
}

PrecisionVariable& Triangulo::heron() {
	PrecisionVariable *res = new PrecisionVariable(precision);
	PrecisionVariable s(precision), radice(precision), suma(precision);
	PrecisionVariable lado1(precision),lado2(precision),lado3(precision);
	lado1 = this->lado1;
	lado2 = this->lado2;
	lado3 = this->lado3;
    
    suma = lado1 + lado2 + lado3;
	s = suma/2;
    radice = s * (s - lado1) * (s - lado2) * (s - lado3);
    
    if (radice.getReal() < 0) {
        _showHeaderError("HERÓN, raiz negativa", lado1, lado2, lado3);
        cout << "  precision=" << precision << "  s=" << s << endl;
        cout << "  radice=" << radice << "  s-lado1=" <<  (s - lado1) << endl;
        cout << "  s-lado2=" <<  (s - lado2) << "  s-lado3=" <<  (s - lado3) << endl;
    }

    // NO FUNCA, muestra este error aún cuando suma == maximo(l1,l2,l3) :-(
    //if ( suma < maximo(lado1,lado2,lado3) ) {
    //    _showHeaderError("HERÓN, suma hiper-truncada", lado1, lado2, lado3);
    //    cout << "   suma=" << suma << " max=" << maximo(lado1,lado2,lado3) << endl;
    //};

	*res = raiz(radice);
	return *res;
}

PrecisionVariable& Triangulo::diferenciasFaciales() {
	PrecisionVariable *res = new PrecisionVariable(precision);
	PrecisionVariable s(precision),radice(precision);
	PrecisionVariable lado1(precision),lado2(precision),lado3(precision);
	lado1 = this->lado1;
	lado2 = this->lado2;
	lado3 = this->lado3;
    radice = (lado1 + lado2 + lado3) * (lado2 - lado3 + lado1) * (lado3 - lado1 + lado2) * (lado1 - lado2 + lado3);
    if (radice.getReal() < 0) {
        cout << "DIF FACIAL, raiz negativa : l1=" << lado1 << "  l2=" << lado2 << "  l3=" << lado3 << endl;
        cout << "  precision=" << precision << endl;
        cout << "  l1+l2+l3=" << (lado1 + lado2 + lado3) << endl;
        cout << "  l2-l3+l1=" << (lado2 - lado3 + lado1) << endl;
        cout << "  l3-l1+l2=" << (lado3 - lado1 + lado2) << endl;
        cout << "  l1-l2+l3=" << (lado1 - lado2 + lado3) << endl;
    }
	*res = raiz(radice)/4;
	return *res;
}

PrecisionVariable& Triangulo::diferenciasFacialesRevisado() {
	PrecisionVariable *res = new PrecisionVariable(precision);
	PrecisionVariable s(precision),radice(precision);
	PrecisionVariable lado1(precision),lado2(precision),lado3(precision);
	lado1 = this->lado1;
	lado2 = this->lado2;
	lado3 = this->lado3;
    radice = (lado1 + lado2 + lado3) * (maximo(lado2,lado1) - lado3 + minimo(lado2,lado1)) * (maximo(lado2,lado3) - lado1 + minimo(lado2,lado3)) * (maximo(lado3,lado1) - lado2 + minimo(lado3,lado1));
    if (radice.getReal() < 0) {
        cout << "DIF FACIAL REV, raiz negativa : l1=" << lado1 << "  l2=" << lado2 << "  l3=" << lado3 << endl;
        cout << "  precision=" << precision << endl;
        cout << "  l1+l2+l3=" << (lado1 + lado2 + lado3) << endl;
        cout << "  max(l2,l1)-l3*min(l1,l2)=" << (maximo(lado2,lado1) - lado3 + minimo(lado2,lado1)) << endl;
        cout << "  max(l2,l3)-l1+min(l2,l3)=" << (maximo(lado2,lado3) - lado1 + minimo(lado2,lado3)) << endl;
        cout << "  max(l3,l1)-l2+min(l3,l1)=" << (maximo(lado3,lado1) - lado2 + minimo(lado3,lado1)) << endl;
    }
	*res = raiz(radice)/4;
	return *res;
}

ostream& operator<< (ostream& out, Triangulo& t) {
	out << "(L1= " << t.getLado1() << ", L2= " << t.getLado2() << ", L3= " << t.getLado3() << ", A= " << t.getArea() << ")" << endl;
	return out;
}

