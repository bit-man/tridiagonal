#include "FamiliaTiendeACero1.h"

list<Triangulo> FamiliaTiendeACero1::getFamilia(unsigned int precision) {
	PrecisionVariable ladoUnidad(precision);
	ladoUnidad = sqrt((long double)3);
	PrecisionVariable A(64);
	A = (sqrt((long double)3)*3)/4;
    familia.clear();

	PrecisionVariable area(64);
	PrecisionVariable lado1(precision);
	for(int i = 1; i <= cantidadMaxima; i++){
		PrecisionVariable ladoVariable(precision);
		ladoVariable = (long double)(ladoUnidad.getReal() / pow((long double)2,(long double)i));
		area = (A.getReal() / (pow((long double)2,(long double)i)))/pow((long double)2,(long double)i);
		Triangulo *t = new Triangulo(precision,ladoVariable,ladoVariable,ladoVariable,area);
		familia.push_back(*t);
	}
	return familia;
}

string FamiliaTiendeACero1::getNombre() {
    return "FamiliaTiendeACero1";
}
