
#include "FamiliaTiendeACero2.h"

list<Triangulo> FamiliaTiendeACero2::getFamilia(unsigned int precision) {
	PrecisionVariable lado1(64);
	PrecisionVariable lado2(64);
	PrecisionVariable lado3(64);
	PrecisionVariable area(64);
	lado1 = 1;
    familia.clear();

	for(int i = 1; i < cantidadMaxima; i++) {
		lado2 = lado1 / (long double)pow((long double)i,(long double)2.5);
		lado3 = raiz(lado2*lado2 + 1);
		area = lado2 / (long double)2;
		Triangulo *t = new Triangulo(precision,lado3,lado1,lado2,area);
		familia.push_back(*t);
	}
	return familia;
}

string FamiliaTiendeACero2::getNombre() {
    return "FamiliaTiendeACero2";
}
