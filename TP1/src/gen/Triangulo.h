

#ifndef TRIANGULO_H_
#define TRIANGULO_H_

#include "../superFloat/PrecisionVariable.h"

using namespace std;

class Triangulo {
private:
	PrecisionVariable lado1,lado2,lado3,area;
	unsigned int precision;

    void _showHeaderError(string, PrecisionVariable&, PrecisionVariable&, PrecisionVariable&);

public:
	Triangulo(unsigned int precision,PrecisionVariable lado1, PrecisionVariable lado2, PrecisionVariable lado3, PrecisionVariable area);
	~Triangulo();
    PrecisionVariable& getLado1();
    PrecisionVariable& getLado2();
    PrecisionVariable& getLado3();
    PrecisionVariable& getArea();
    PrecisionVariable& heron();
    PrecisionVariable& diferenciasFaciales() ;
    PrecisionVariable& diferenciasFacialesRevisado() ;

    friend ostream& operator<< (ostream& , Triangulo& );

};

#endif /* TRIANGULO_H_ */
