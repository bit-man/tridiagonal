#include <iostream>
#include <cmath>
#include "PrecisionVariable.h"

using namespace std;

PrecisionVariable::PrecisionVariable() {
	precision = 0;
}

PrecisionVariable::PrecisionVariable(unsigned int precision) {
	this->precision = precision;
}

PrecisionVariable& PrecisionVariable::operator +(PrecisionVariable &sumando) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real + sumando.real;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& PrecisionVariable::operator +(BIG_NUMBER num) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real + num;
	res->real = truncar(res->real);
	return *res;
}


PrecisionVariable& PrecisionVariable::operator -(PrecisionVariable &sustraendo) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real - sustraendo.real;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& PrecisionVariable::operator -(BIG_NUMBER sustraendo) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real - sustraendo;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& PrecisionVariable::operator =(BIG_NUMBER num) {
	this->real = truncar(num);
	return *this;
}

PrecisionVariable& PrecisionVariable::operator=(PrecisionVariable& num) {
	this->real = num.real;
	return *this;
}

bool operator == ( PrecisionVariable& num1, PrecisionVariable& num2) {
	return (num1.precision == num2.precision) && (num1.real == num2.real);
}

bool operator < ( PrecisionVariable& num1, PrecisionVariable& num2) {
	return minimo(num1, num2) == num1;
}

bool operator > ( PrecisionVariable& num1, PrecisionVariable& num2) {
	return minimo(num1, num2) == num2;
}

ostream& operator<< (ostream& out, PrecisionVariable& num) {
    char buf[30];
    sprintf(buf,"%.20LE",num.getReal());
    out << buf;

    return out;
}

BIG_NUMBER PrecisionVariable::truncar(BIG_NUMBER num) {
	BIG_NUMBER mantisa;
	int exponente;
	mantisa = frexp(num,&exponente);

    /**
     * 1.0 < mantisa <= 0.5
     * Al multiplicarla por 2^precisión necesaria equivale a
     * correr la coma a la derecha precisión lugares, dejando
     * en la parte entera de la mantisa los bits corrspondientes
     * a la precisión con la que desea trabajar
     *
     * ej. mantisa = 0,11010110...1 , precision = 4
     *     mantisa * 2^precision = 1101,0100...1
     *
     **/
	BIG_NUMBER mantisax2k = ldexp(mantisa,this->precision);
	mantisa = floor(mantisax2k);

    /**
     * Como la coma de la mantisa fue corrida n lugares a la derecha
     * esto es lo mismo que multiplicarla por 2^n, por lo tanto el
     * exponente se vio disminuido en n lugares
     *
     * ej.  0,11010110 * 2^7 = 0,11010110 * 2^(4+3) = 0,11010110 * 2^4 * 2^3
     *    =   1101,0110 * 2^3
     *
     **/
	BIG_NUMBER truncado = ldexp(mantisa,exponente - this->precision);

	return truncado;
}

BIG_NUMBER PrecisionVariable::getReal() {
	return this->real;
}

PrecisionVariable& PrecisionVariable::operator *(PrecisionVariable& num) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real * num.real;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& PrecisionVariable::operator *(BIG_NUMBER num) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real * num;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& PrecisionVariable::operator /(PrecisionVariable& num) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real / num.real;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& PrecisionVariable::operator /(BIG_NUMBER num) {
	PrecisionVariable *res = new PrecisionVariable(this->precision);
	res->real = this->real / num;
	res->real = truncar(res->real);
	return *res;
}

PrecisionVariable& minimo(PrecisionVariable& num1,PrecisionVariable& num2) {
	if(num1.real < num2.real) {
		return num1;
	} else {
		return num2;
	}
}


PrecisionVariable& maximo(PrecisionVariable& num1, PrecisionVariable& num2,
                          PrecisionVariable& num3) {
    return maximo( num1, maximo(num2, num3) );
}

PrecisionVariable& maximo(PrecisionVariable& num1,PrecisionVariable& num2) {
	if(num1.real < num2.real) {
		return num2;
	} else {
		return num1;
	}
}

PrecisionVariable& raiz(PrecisionVariable& num) {
	PrecisionVariable *res = new PrecisionVariable(num.precision);
	*res = sqrt(num.real);
	return *res;
}

unsigned int PrecisionVariable::getPrecision() {
	return precision;
}

PrecisionVariable& abs(PrecisionVariable& num) {
    PrecisionVariable *res = new PrecisionVariable(num.precision);

    if (num.getReal() < 0 ) {
        *res = num * (long double) -1 ;
    } else {
        *res = num;
    }

    return *res;
}

void PrecisionVariable::setPrecision(unsigned int precision) {
	this->precision = precision;
}


