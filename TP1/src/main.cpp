#include <iostream>
#include <cmath>
#include <limits>
#include <fstream>
#include <sstream>
#include <string>

#include "superFloat/PrecisionVariable.h"
#include "gen/FamiliaTriangulos.h"
#include "gen/FamiliaTiendeACero1.h"
#include "gen/FamiliaTiendeACero2.h"
#include "gen/FamiliaTiendeACero3.h"
#include "gen/FamiliaPiolin.h"

using namespace std;

#define SEP_CAMPO       " "
#define EXT_DEFAULT     ".csv"
#define FAMILIA         "_fam_"
#define CALCULO         "_calc_"

#define OCUPA_ESPACIO   "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
#define TP1_HEADER      "TP1: "

void grabarEncabezamientoFamilia(ofstream& archivo) {
    archivo << "\"Lado 1\"" << SEP_CAMPO << "\"Lado 2\"" << SEP_CAMPO;
    archivo << "\"Lado 3\"" << SEP_CAMPO << "\"# triángulo\"" << endl;
}


void grabarEncabezamientoResultados(ofstream& archivo) {
        archivo << "\"Herón\"" << SEP_CAMPO << "\"Error relativo\"" << SEP_CAMPO;
        archivo << "\"Dif. Faciales\"" << SEP_CAMPO << "\"Error relativo\"" << SEP_CAMPO;
        archivo << "\"Dif. Faciales revisado\"" << SEP_CAMPO;
        archivo << "\"Error Relativo\"" << SEP_CAMPO << "\"Superficie\"" << SEP_CAMPO;
        archivo << "\"# triángulo\"" << endl;
}


// Graba la familia generada en un archivo
void grabarFamilia(FamiliaTriangulos *f, unsigned int precision) {
    char ext[] = EXT_DEFAULT;
    string nomFamilia = f->getNombre();
    ofstream fsalida;
    stringstream archName;
    archName << nomFamilia << FAMILIA << precision << ext;
    // TODO: hack horrible
    char archName2[] = OCUPA_ESPACIO;
    archName >> archName2;

    fsalida.open(archName2);
    list<Triangulo> familia = f->getFamilia(precision);
    list<Triangulo>::iterator it = familia.begin();

    cout << TP1_HEADER << "Grabando familia '" << nomFamilia << "'en archivo '" << archName2 << "'" << endl;
    grabarEncabezamientoFamilia(fsalida);
    int numTriang = 1;
    while(it != familia.end()){
        fsalida << it->getLado1() << SEP_CAMPO << it->getLado2() << SEP_CAMPO << it->getLado3() <<  SEP_CAMPO << numTriang << endl;
        it++;
        numTriang++;
    }

    fsalida.close();
}

void calcular(FamiliaTriangulos *f) {
    char ext[] = EXT_DEFAULT;
    
    for( int precision = MIN_PRECISION; precision <= MAX_PRECISION; precision++) {

        grabarFamilia(f,precision);

        list<Triangulo> familia = f->getFamilia(precision);

        string salida = f->getNombre();
        ofstream archivo;
        stringstream archName;
        archName << salida << CALCULO << precision  <<  ext;
        // TODO: hack horrible
        char archName2[] = OCUPA_ESPACIO;
        archName >> archName2;
        archivo.open(archName2);

        cout << TP1_HEADER << "Precisión : " << precision << " -- " << archName2 << endl;
        grabarEncabezamientoResultados(archivo);
        
        list<Triangulo>::iterator it = familia.begin();
        int i = 1;
        while(it != familia.end()){
            PrecisionVariable heron(precision), difFacial(precision), difFacialR(precision);
            PrecisionVariable errH(MAX_PRECISION),errDF(MAX_PRECISION),errDFR(MAX_PRECISION);

            heron = it->heron();
            difFacial = it->diferenciasFaciales();
            difFacialR = it->diferenciasFacialesRevisado();
            errH =  abs( ( heron - it->getArea() ) / it->getArea() );
            errDF = abs( ( difFacial - it->getArea() ) / it->getArea() );
            errDFR = abs( ( difFacialR - it->getArea() ) / it->getArea() );

            archivo << heron << SEP_CAMPO << errH << SEP_CAMPO ;
            archivo << difFacial << SEP_CAMPO << errDF << SEP_CAMPO;
            archivo << difFacialR << SEP_CAMPO << errDFR << SEP_CAMPO;
            archivo << it->getArea() << SEP_CAMPO << i << endl;

            it++;
            i++;
        }
        
        archivo.close();
    }
}

int main( int argc, char *argv[] ) {
    FamiliaTriangulos *f = new FamiliaPiolin();
    calcular(f);

    f = new FamiliaTiendeACero1();
    calcular(f);

    f = new FamiliaTiendeACero2();
    calcular(f);

    f = new FamiliaTiendeACero3();
    calcular(f);

}

