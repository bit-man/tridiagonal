/*
 * MetodoQR.cpp
 *
 *  Created on: 03/06/2009
 *      Author: cgauna
 */

#include "MetodoQR.h"

MetodoQR::MetodoQR() {
	_autovaloresCalculados = false;

}

MetodoQR::~MetodoQR() {
	// TODO Auto-generated destructor stub
}

void MetodoQR::factorizarQR(MatrizFullFull & matriz) {
	CONTADOR dim = matriz.getFilas();
	Q.redimensionar(dim,dim);
	FLOTANTE senos[dim-1];
	FLOTANTE cosenos[dim-1];
	FLOTANTE valor,x,y,norma,coseno,seno,xd,yd,z = 0.0;
	//Construyo la matriz R
	R = matriz;
	for(CONTADOR i = 0; i < dim-1; i++) {
		x = R.get(i,i);
		y = R.get(i+1,i);
		xd = R.get(i,i+1);
		yd = R.get(i+1,i+1);
		norma = sqrt(pow(x,2) + pow(y,2));
		coseno = x / norma;
		seno = y / norma;
		valor = x*coseno + y*seno;
		R.set(i,i,valor);
		valor = xd*coseno + yd*seno;
		R.set(i,i+1,valor);
		if(i < dim-2) {
			z = R.get(i+1,i+2);
			valor = z*seno;
			R.set(i,i+2,valor);
		}
		valor = x*(-seno) + y*coseno;
		R.set(i+1,i,valor);
		valor = xd*(-seno) + yd*coseno;
		R.set(i+1,i+1,valor);
		if(i < dim-2) {
			valor = z*coseno;
			R.set(i+1,i+2,valor);
		}
		//Los senos y los cosenos los almaceno en orden inverso para despues calcular
		//la matriz Q con las inversas de las matrices de rotacion
		cosenos[dim-i-2] = coseno;
		senos[dim-i-2] = seno;
	}
	//Contruyo la matriz Q
	Q.set(dim-2,dim-2,cosenos[0]);
	Q.set(dim-2,dim-1,-senos[0]);
	Q.set(dim-1,dim-2,senos[0]);
	Q.set(dim-1,dim-1,cosenos[0]);
	for(CONTADOR i = 0; i < dim-2; i++) {
		Q.set(dim-i-3,dim-i-3,cosenos[i+1]);
		Q.set(dim-i-2,dim-i-3,senos[i+1]);
		for(CONTADOR j = dim-i-2; j < dim; j++) {
			valor = Q.get(dim-i-2,j)*(-senos[i+1]);
			Q.set(dim-i-3,j,valor);
			valor = Q.get(dim-i-2,j)*(cosenos[i+1]);
			Q.set(dim-i-2,j,valor);
		}
	}
}

MatrizFullFull & MetodoQR::getQ() {
	return Q;
}

MatrizFullFull & MetodoQR::getR() {
	return R;
}

void MetodoQR::convertirSimetrica(MatrizFullFull & matriz) {
	CONTADOR dim = matriz.getFilas();
	FLOTANTE coeficientes[dim];
	coeficientes[0] = 1;
	FLOTANTE ij,ji,nuevo;
	for(CONTADOR i = 0; i < dim-1; i++) {
		ij = matriz.get(i,i+1);
		ji = matriz.get(i+1,i);
		nuevo = sqrt(ji/ij)*coeficientes[i];
		coeficientes[i+1] = nuevo;
	}
	for(CONTADOR i = 1; i < dim-1; i++) {
		nuevo = (matriz.get(i,i-1)/coeficientes[i])*coeficientes[i-1];
		matriz.set(i,i-1,nuevo);
		nuevo = (matriz.get(i,i+1)/coeficientes[i])*coeficientes[i+1];
		matriz.set(i,i+1,nuevo);
	}
	nuevo = (matriz.get(0,1)/coeficientes[0])*coeficientes[1];
	matriz.set(0,1,nuevo);
	nuevo = (matriz.get(dim-1,dim-2)/coeficientes[dim-1])*coeficientes[dim-2];
	matriz.set(dim-1,dim-2,nuevo);
}

FLOTANTE * MetodoQR::getAutovalores(CONTADOR iteraciones, MatrizFullFull & matriz, FLOTANTE presicion) {
	convertirSimetrica(matriz);
	factorizarQR(matriz);
	FLOTANTE ultimo = -matriz.get(0,0);
	for(CONTADOR i = 0; i < iteraciones; i++) {
		MatrizFullFull & res = multiplicarTridiagonales(getR(),getQ());
		factorizarQR(res);
		if(-(ultimo + res.get(0,0)) < presicion) {
			break;
		} else {
			ultimo = -res.get(0,0);
		}
		res.~MatrizFullFull();
	}
	MatrizFullFull & res = multiplicar(getR(),getQ());
	autovalores = new FLOTANTE[res.getFilas()];
	for(CONTADOR i = 0; i < res.getFilas(); i++) {
		autovalores[i] = res.get(i,i);
	}

	_nAutovalores = res.getFilas();
	_autovaloresCalculados = true;
	res.~MatrizFullFull();
	return autovalores;
}

FLOTANTE * MetodoQR::getAutovalores(CONTADOR iteraciones,MatrizFullFull & matriz) {
	return getAutovalores(iteraciones,matriz,PRESICION);
}


FLOTANTE * MetodoQR::getFrecuencias() {

	for(CONTADOR i = 0; i < _nAutovalores; i++ ) {
		autovalores[i] = sqrt( -1 * autovalores[i] );
	};

	return autovalores;
}


FLOTANTE * MetodoQR::getFrecuencias(CONTADOR iteraciones, MatrizFullFull & matriz) {
	getAutovalores(iteraciones, matriz);
	return getFrecuencias();
}

void MetodoQR::mostrarFrecuencias(CONTADOR iteraciones,MatrizFullFull & matriz) {
	FLOTANTE * frecuencias = getAutovalores(iteraciones,matriz);
	cout << endl << "frecuencias: " << endl;
	for(CONTADOR i = 0; i < matriz.getFilas(); i++) {
		cout << frecuencias[i] << " ";
	}
	cout << endl;
	delete[] frecuencias;
}
