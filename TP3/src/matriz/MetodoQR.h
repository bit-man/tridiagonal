/*
 * MetodoQR.h
 *
 *  Created on: 03/06/2009
 *      Author: cgauna
 */

#ifndef METODOQR_H_
#define METODOQR_H_

#include <vector>
#include "../tp3/TiposBasicos.h"
#include "MatrizFullFull.h"
#include <cmath>

/***
 * Devuelve los autovalores de la matriz utilizada, utilizando el métodoQR
 * la cantidad de veces indicada por el parámetro ietraciones.
 * Debe llamarse a getAutovalores(...) y luego, si desean calcularse lsd
 * frecuencias, llamar a getFrecuencias(). También puede llamarse sólo
 * a getFrecuencias(...) que realiza todo el cálculo y obtener las frecuencias
 */
class MetodoQR {
private:
	MatrizFullFull Q,R;
	FLOTANTE*  autovalores;
	// indica si los autovalores fueron calculados
	FLAG _autovaloresCalculados;
	CONTADOR _nAutovalores;
	static const FLOTANTE PRESICION = 0.000001;

public:
	MetodoQR();
	virtual ~MetodoQR();

	FLOTANTE * getAutovalores(CONTADOR iteraciones, MatrizFullFull & matriz);
	FLOTANTE * getFrecuencias(CONTADOR iteraciones, MatrizFullFull & matriz);
	FLOTANTE * getAutovalores(CONTADOR iteraciones, MatrizFullFull & matriz,FLOTANTE presicion);
	/***
	 * Calcula las frecuencias naturales en base a los autovalores.
	 * Las frecuencias se encuentran ordenadas de menor a mayor
	 */

	FLOTANTE * getFrecuencias();

	//Precondicion: Q y R son la factorizacion QR de una matriz tridiagonal
	//void multiplicar(MatrizFullFull & Q, MatrizFullFull & R);

	void factorizarQR(MatrizFullFull & matriz);

	MatrizFullFull & getQ();

	MatrizFullFull & getR();

	void convertirSimetrica(MatrizFullFull & matriz);
	void mostrarFrecuencias(CONTADOR iteraciones,MatrizFullFull & matriz);
};

#endif /* METODOQR_H_ */
