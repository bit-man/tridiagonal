/*
 * MatrizFullFull.h
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */


#ifndef __MATRIZFULLFULL_H
#define __MATRIZFULLFULL_H

#include "MatrizBase.h"

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

/***
 * Clase base para la implementación de una matrices para resolución por
 * métodos de triangulación (ej. Gauss)
 **/
class MatrizFullFull : public MatrizBase {

private:
    bool coincideCon(MatrizFullFull &m);

public:

    MatrizFullFull();
    MatrizFullFull (unsigned int n, unsigned int m);
    MatrizFullFull (const MatrizFullFull & M);
    MatrizFullFull (const FLOTANTE * m, unsigned int filas, unsigned int columnas);
    MatrizFullFull (unsigned int n);
    ~MatrizFullFull ();
    void mostrar();
    const FLOTANTE get(unsigned int i, unsigned int j);
    const FLOTANTE get(unsigned int i);
    void set(unsigned int i, unsigned int j, FLOTANTE x);
    void set(unsigned int i, unsigned int j, int x);
    void set(unsigned int i, FLOTANTE x);
    void set(unsigned int i, int x);
    MatrizFullFull &operator= (MatrizFullFull &M);
    friend bool operator== (MatrizFullFull& a, MatrizFullFull& b) ;
    bool esParecidaA (MatrizFullFull &m, FLOTANTE error);

    void sumar(unsigned int i, unsigned int j, FLOTANTE x);

    void redimensionar(unsigned int n, unsigned int m);
    void redimensionar(unsigned int n);

    unsigned int getFilas();
    unsigned int getColumnas();

    void transponer();
};

MatrizFullFull & multiplicar(MatrizFullFull & m1, MatrizFullFull & m2);
MatrizFullFull & multiplicarTridiagonales(MatrizFullFull & m1, MatrizFullFull & m2);
void generarArchivoMatLab(const char* nombreMatriz,const char* nombreArchivo,MatrizFullFull & matriz);

#endif /*__MATRIZFULLFULL_H */

