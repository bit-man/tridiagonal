/*
 * MatrizFullFull.cpp
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */


#include "MatrizFullFull.h"
#include "../tp3/TiposBasicos.h"


using namespace std;


/***
 * Constructor vacío para cuando se necesita sólo declarar una
 * variable sin aún indicar sus dimensiones
 */
MatrizFullFull::MatrizFullFull(): MatrizBase::MatrizBase() {
}

MatrizFullFull::MatrizFullFull (unsigned int n, unsigned int m) :
	MatrizBase::MatrizBase(n,m) {
}

MatrizFullFull::MatrizFullFull (const MatrizFullFull& M) :
	MatrizBase::MatrizBase(M) {
}

MatrizFullFull::MatrizFullFull (const FLOTANTE * m, unsigned int filas, unsigned int columnas) {
	redimensionar(filas,columnas);

	for (unsigned int i = 0; i < filas; i++) {
		for (unsigned int j = 0; j < columnas; j++) {
			set(i,j, m[filas*i+j]);
		}
	}
}

MatrizFullFull::MatrizFullFull (unsigned int n) :
	MatrizBase::MatrizBase(n) {
}

void MatrizFullFull::redimensionar(unsigned int n) {
	MatrizBase::redimensionar(n);
}

void MatrizFullFull::redimensionar(unsigned int n, unsigned int m) {
	MatrizBase::redimensionar(n,m);
}


void MatrizFullFull::set(unsigned int i, unsigned int j, FLOTANTE x) {
	MatrizBase::set(i,j,x);
}

const FLOTANTE MatrizFullFull::get(unsigned int i, unsigned int j) {
	return MatrizBase::get(i,j);
}

const FLOTANTE MatrizFullFull::get(unsigned int i) {
	return MatrizBase::get(i);
}

void MatrizFullFull::set(unsigned int i, unsigned int j, int x) {
	MatrizBase::set(i,j, x);
}

/**
 * Coloca el valor X en el elemento 'i', suponiendo que la
 * matriz es una matriz columna.
 */
void MatrizFullFull::set(unsigned int i, FLOTANTE x) {
	MatrizBase::set(i,x);
}

void MatrizFullFull::set(unsigned int i, int x) {
	MatrizBase::set(i,x);
}

void MatrizFullFull::mostrar() {
	MatrizBase::mostrar();
}


void MatrizFullFull::sumar(unsigned int i, unsigned int j, FLOTANTE x) {
	FLOTANTE temp = this->get(i,j) + x;
	this->set(i, j, temp);
}

unsigned int MatrizFullFull::getFilas() {
	return MatrizBase::getFilas();
}

unsigned int MatrizFullFull::getColumnas() {
	return MatrizBase::getColumnas();
}

MatrizFullFull::~MatrizFullFull() {
	liberar();
}

MatrizFullFull &MatrizFullFull::operator= (MatrizFullFull& M) {
	redimensionar(M.filas, M.columnas);
	for (unsigned int i = 0; i < filas; i++) {
		for (unsigned int j = 0; j < columnas; j++) {
			set(i,j, M.get(i,j));
		}
	}

  return *this;
}

bool operator== (MatrizFullFull& a, MatrizFullFull& b) {
	return (a.filas == b.filas && a.columnas == b.columnas &&
			a.coincideCon(b) );
}

bool MatrizFullFull::coincideCon(MatrizFullFull& m) {
	return this->esParecidaA(m, (FLOTANTE) 0);
}


bool MatrizFullFull::esParecidaA (MatrizFullFull &m, FLOTANTE error) {
	bool sonParecidas = true;

	for( unsigned int i = 0; i < filas && sonParecidas; i++) {
		for( unsigned int j=0; j < columnas && sonParecidas; j++ ) {
			sonParecidas = fabs(this->get(i,j) - m.get(i,j)) <= error;
		}
	}

	return sonParecidas;
}

void MatrizFullFull::transponer() {
	for(CONTADOR i = 0; i < getFilas(); i++) {
		for(CONTADOR j = i + 1; j < getFilas(); j++) {
			FLOTANTE valor = get(i,j);
			set(i,j,get(j,i));
			set(j,i,valor);
		}
	}
}

MatrizFullFull & multiplicar(MatrizFullFull & m1, MatrizFullFull & m2) {
	unsigned int dim = m1.getFilas();
	MatrizFullFull * res = new MatrizFullFull(dim,dim);
	FLOTANTE acum = 0.0;
	for(CONTADOR i = 0; i < dim; i++) {
		for(CONTADOR j = 0; j < dim; j++) {
			for(CONTADOR k = 0; k < dim; k++) {
				acum += m1.get(i,k) * m2.get(k,j);
			}
			res->set(i,j,acum);
			acum = 0;
		}
	}

	return *res;
}

MatrizFullFull & multiplicarTridiagonales(MatrizFullFull & m1, MatrizFullFull & m2) {
	unsigned int dim = m1.getFilas();
	MatrizFullFull * res = new MatrizFullFull(dim,dim);
	FLOTANTE acum = 0.0;

	for(CONTADOR i = 0; i < dim-1; i++) {
		for(CONTADOR j = i; j < i+2; j++) {
			for(CONTADOR k = 0; k < dim; k++) {
				acum += m1.get(i,k) * m2.get(k,j);
			}
			res->set(i,j,acum);
			if(i != j) {
				res->set(j,i,acum);
			}
			acum = 0;
		}
	}
	CONTADOR ult = m1.getFilas()-1;
	for(CONTADOR i = 0; i < m1.getFilas(); i++) {
		acum += m1.get(ult,i) * m2.get(i,ult);
	}
	res->set(ult,ult,acum);
	return *res;
}

void generarArchivoMatLab(const char* nombreMatriz,const char* nombreArchivo,MatrizFullFull & matriz) {
	FILE * fp=fopen(nombreArchivo,"w+");
	fprintf(fp,"# name: %s\n# type: matrix\n# rows: %d\n# columns: %d\n",nombreMatriz,matriz.getFilas(),matriz.getFilas());
	for(CONTADOR i = 0; i < matriz.getFilas(); i++) {
		for(CONTADOR j = 0; j < matriz.getFilas(); j++) {
			fprintf(fp," %.10LF",matriz.get(i,j));
		}
		fprintf(fp,"\n");
	}
	fclose(fp);
}
