/*
 * MatrizBase.h
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */


#ifndef __MATRIZBASE_H
#define __MATRIZBASE_H

#define FLOTANTE long double
#define NULO 0;
#include <iostream>

using namespace std;

/***
 * Clase base para la implementación de una matrices para resolución por
 * métodos de triangulación (ej. Gauss)
 **/
class MatrizBase {
protected:
   unsigned int filas, columnas;
   FLOTANTE **miMatriz;
   virtual void liberar();

private:
   virtual void inicializar ();
  // virtual bool coincideCon(MatrizBase &m) =0;

public:

    MatrizBase();
    MatrizBase (unsigned int n, unsigned int m);
    MatrizBase (const MatrizBase & M);
    MatrizBase (unsigned int n);
    virtual ~MatrizBase ();
    virtual void mostrar();
   virtual const FLOTANTE get(unsigned int i, unsigned int j);
   virtual const FLOTANTE get(unsigned int i);
   virtual void set(unsigned int i, unsigned int j, FLOTANTE x);
   virtual void set(unsigned int i, unsigned int j, int x);
   virtual void set(unsigned int i, FLOTANTE x);
   virtual void set(unsigned int i, int x);
   //virtual MatrizBase &operator= (MatrizBase &M) =0;
   friend bool operator== (MatrizBase& a, MatrizBase& b) ;
   bool esParecidaA (MatrizBase &m, FLOTANTE error);

   virtual unsigned int getFilas();
   virtual unsigned int getColumnas();

   virtual void sumar(unsigned int i, unsigned int j, FLOTANTE x);

   virtual void redimensionar(unsigned int n, unsigned int m);
   virtual void redimensionar(unsigned int n);

} ;

#endif /*__MATRIZBASE_H */

