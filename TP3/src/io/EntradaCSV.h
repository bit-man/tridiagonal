/*
 * EntradaCSV.h
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef ENTRADACSV_H_
#define ENTRADACSV_H_

#include <iostream>
#include <fstream>

#include <sstream>
#include <string>

#define SEPARADOR_DEFAULT     ','

using namespace std;

class EntradaCSV : public ifstream {
private:
	char _separador;
	bool _consumirSeparador();
	bool _consumirChar(char c);
public:
	EntradaCSV(const char *nombre);
	bool hayMasDatosEnEstaLinea();
	void siguienteLinea();
	void separador(char c);
	long double leerReal();
	float leerFloat();
	int leerEntero();
	string leerCaracteres();
	virtual ~EntradaCSV();
};

#endif /* ENTRADACSV_H_ */
