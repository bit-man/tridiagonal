/*
 * Entrada.h
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef ENTRADA_H_
#define ENTRADA_H_

#include <iostream>
#include "EntradaCSV.h"
#include "../tp3/TiposBasicos.h"
#include "../matriz/MatrizFullFull.h"

#define SEPARADOR     ' '

using namespace std;

class Entrada : public EntradaCSV {
private:
    CONTADOR _n;
    FLOTANTE _p;
    FLOTANTE _m0;
    FLOTANTE *_k;
    CONTADOR * _lavarropas;
    FLAG _limpiarK;
    FLAG _nLeido;
    FLAG _pLeido;
    FLAG _m0Leido;
    FLAG _kLeido;
    FLAG _lavarropasLeido;
    FLAG _limpiarLavarropas;

	NADA _leerMatriz();
	NADA _leerDatoEntero(CONTADOR & datum, FLAG & leido);
    NADA _leerDatoReal(FLOTANTE & datum, FLAG & leido);

public:
	Entrada(const char *nombre): EntradaCSV(nombre) {
		_nLeido = false;
        _pLeido = false;
        _m0Leido = false;
        _kLeido = false;
        _limpiarK = false;
        _limpiarLavarropas = false;
        _lavarropasLeido = false;
        separador(SEPARADOR);
	};
    CONTADOR pisos();
    FLOTANTE masaLavarropas();
    FLOTANTE masaDelPiso();
    void generarMatriz(MatrizFullFull & matriz);
	virtual ~Entrada();
	// los siguientes métodos, si bien públicos, son sólo de uso interno y se
	// exponen sólo a los efectos de poder probar la correcta lectura de los mismos
    FLOTANTE * _coeficientesDeRigidez();
    CONTADOR * _lavarropasPorPiso();
};

#endif /* ENTRADA_H_ */
