/*
 * Entrada.cpp
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "Entrada.h"


Entrada::~Entrada() {
    if ( _limpiarK ) {
        delete[] _k;
    }
    if ( _limpiarLavarropas ) {
        delete[] _lavarropas;
    }
}

CONTADOR Entrada::pisos() {
    _leerDatoEntero(_n, _nLeido);
    return _n;
};


FLOTANTE Entrada::masaLavarropas() {
    pisos();
    _leerDatoReal(_p, _pLeido);
    return _p;
};

FLOTANTE Entrada::masaDelPiso() {
    pisos();
    masaLavarropas();

    FLAG _estabaLeido = _m0Leido;
    _leerDatoReal(_m0, _m0Leido);
    if ( ! _estabaLeido ) {
        siguienteLinea();
    }

    return _m0;
};


FLOTANTE * Entrada::_coeficientesDeRigidez() {
    CONTADOR n = pisos();
    masaLavarropas();
    masaDelPiso();

    if ( ! _kLeido ) {
        _limpiarK = true;
        _k = new FLOTANTE[n];
        for( CONTADOR i = 0; i < n; i++ ) {
            _k[i] = leerReal();
        };
        _kLeido = true;
        siguienteLinea();
    }

    return _k;
}

CONTADOR * Entrada::_lavarropasPorPiso() {
    CONTADOR n = pisos();
    masaLavarropas();
    masaDelPiso();
    _coeficientesDeRigidez();

    if ( ! _lavarropasLeido ) {
        _limpiarLavarropas = true;
        _lavarropas = new CONTADOR[n];
        for( CONTADOR i = 0; i < n; i++ ) {
            _lavarropas[i] = leerEntero();
        };
        _lavarropasLeido = true;
    }
    return _lavarropas;
}


NADA Entrada::_leerDatoReal(FLOTANTE & datum, FLAG & leido) {
	if (! leido) {
		datum = leerReal();
		leido = true;
	}
}

NADA Entrada::_leerDatoEntero(CONTADOR & datum, FLAG & leido) {
	if (! leido) {
		datum = leerEntero();
		leido = true;
	}
}

void Entrada::generarMatriz(MatrizFullFull & matriz) {
	_lavarropasPorPiso();
	CONTADOR n = pisos();
	//MatrizFullFull * matriz = new MatrizFullFull(n,n);
	//seteo la fila 0
	FLOTANTE masaPiso = _m0 + (_p * (FLOTANTE)(_lavarropas[0]));
	FLOTANTE valor = (-_k[0]-_k[1]) / masaPiso;
	matriz.set(0,0,valor);
	valor = _k[1] / masaPiso;
	matriz.set(0,1,valor);

	//Seteo las filas 1 to n-2
	for(CONTADOR i = 1; i < n-1; i++) {
		masaPiso = _m0 + (_p * (FLOTANTE)(_lavarropas[i]));
		valor = _k[i] / masaPiso;
		matriz.set(i,i-1,valor);
		valor = (-_k[i]-_k[i+1]) / masaPiso;
		matriz.set(i,i,valor);
		valor = _k[i+1] / masaPiso;
		matriz.set(i,i+1,valor);
	}

	//seteo la fila n-1
	masaPiso = _m0 + (_p * (FLOTANTE)(_lavarropas[n-1]));
	valor = _k[n-1] / masaPiso;
	matriz.set(n-1,n-2,valor);
	valor = -_k[n-1] / masaPiso;
	matriz.set(n-1,n-1,valor);

	//return *matriz;
}
