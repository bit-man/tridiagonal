/*
 * EntradaCSV.cpp
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "EntradaCSV.h"

#define FIN_STRING            '"'
#define INICIO_STRING         FIN_STRING
#define BARRA_DE_ESCAPE        '\\'

EntradaCSV::EntradaCSV(const char *nombre) {
	open(nombre);
	_separador = SEPARADOR_DEFAULT;
}

/***
 * Permite consumir el siguiente caracter si es un separador. En caso
 * contrario no lo consume (siempre deja el archivo posicionado en el
 * pr�ximo campo a leer, fin de la linea o fin del archivo)
 */
bool EntradaCSV::_consumirSeparador() {
	return _consumirChar(_separador);
}

bool EntradaCSV::_consumirChar(char charAconsumir) {
	char c;
	bool consumido;

	c = peek();
	if (c == charAconsumir) {
        //Se usa get() y no operator>> porque este puede consumir más de un caracter
		get(c) ;
		consumido = true;
	} else {
		consumido = false;
	}

	return consumido;
}


bool EntradaCSV::hayMasDatosEnEstaLinea(){
	char c;

	c = peek();
	return ! (c == '\n' || c == EOF || eof() );
}

/***
 * lee el pr�ximo dato disponible en la l�nea. Para que la operaci�n no
 * resulte en error debe haberse verificado que hay m�s datos con la
 * operaci�n hayMasDatosEnEstaLinea()
 */
long double EntradaCSV::leerReal() {
	// Tanto float como double se representan igual en format texto
	return (long double) leerFloat();
}

float EntradaCSV::leerFloat() {
	float dato;
	*this >> dato;
    _consumirSeparador();

	return dato;
}

int EntradaCSV::leerEntero() {
	int dato;
	*this >> dato ;
    _consumirSeparador();

	return dato;
}

void EntradaCSV::separador(char c) {
	_separador = c;
}

void EntradaCSV::siguienteLinea() {
	char c = peek();

    while( c != '\n' && ! eof()) {
        //Se usa get() y no operator>> porque este puede consumir más de un caracter
        get(c);
		c = peek();
    };

    // Se consume el '\n'
	if (! eof()) {
        //Se usa get() y no operator>> porque este puede consumir más de un caracter
		get(c);
	}
}

string EntradaCSV::leerCaracteres() {
	string str;
    stringstream aux;
    char c;
	bool entreComillas, seguirLeyendo, escape;

	entreComillas = _consumirChar(INICIO_STRING);

	aux << "";
	seguirLeyendo = true;
	escape = false;
	while( seguirLeyendo ) {
        //Se usa get() y no operator>> porque este puede consumir más de un caracter
		get(c);
		if ( c == BARRA_DE_ESCAPE ) {
			escape = true;
			seguirLeyendo = true;
		} else if ( c == FIN_STRING && ! escape ) {
			putback(c);
			seguirLeyendo = false;
		} else {
			aux.put(c);
			escape = false;
			seguirLeyendo = true;
		}
	}

	aux >> str;

	_consumirChar(FIN_STRING);
	_consumirSeparador();

	return str;
}

EntradaCSV::~EntradaCSV() {
	if (is_open()) {
		close();
	}
}
