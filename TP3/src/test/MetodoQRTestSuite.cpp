/*
 * MetodoQRTestSuite.cpp
 *
 *  Created on: Jun 6, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "MetodoQRTestSuite.h"

#define ERROR_ACEPTADO    0.0000001

/**
 ./TP data/catedra/prueba10.txt
Matriz original
   -41.48148   21.11111   0   0   0   0   0   0   0   0
   20.07042   -41.37324   21.30282   0   0   0   0   0   0   0
   0   21.84116   -45.48736   23.64621   0   0   0   0   0   0
   0   0   21.97987   -38.92617   16.94631   0   0   0   0   0
   0   0   0   18.23105   -36.28159   18.05054   0   0   0   0
   0   0   0   0   18.51852   -40.74074   22.22222   0   0   0
   0   0   0   0   0   21.66065   -45.12635   23.46570   0   0
   0   0   0   0   0   0   24.07407   -38.88889   14.81481   0
   0   0   0   0   0   0   0   14.08451   -31.69014   17.60563
   0   0   0   0   0   0   0   0   18.05054   -18.05054

Los autovalores de la matriz original son
-80.4821 -77.0016 -60.9729 -52.1372 -42.5893 -31.1215 -19.5855 -9.88949 -3.81133 -0.455721
Movimiento efectuado:
   -49.12281   25.00000   0   0   -0   -0   -0   0   0   -0
   17.48466   -36.04294   18.55828   -0   0   0   -0   -0   0   0
   0   21.84116   -45.48736   23.64621   -0   0   0   -0   -0   0
   0   -0   21.97987   -38.92617   16.94631   -0   0   0   0   0
   0   -0   -0   18.23105   -36.28159   18.05054   -0   0   -0   -0
   0   -0   -0   0   18.51852   -40.74074   22.22222   -0   -0   0
   -0   0   -0   0   -0   21.66065   -45.12635   23.46570   -0   -0
   0   -0   0   -0   0   -0   24.07407   -38.88889   14.81481   0
   0   -0   0   -0   0   -0   0   14.08451   -31.69014   17.60563
   0   -0   0   -0   0   -0   0   -0   18.05054   -18.05054

Los autovalores de la matriz cambiada son
-79.8608 -76.2618 -62.36 -53.5032 -43.2936 -31.6059 -19.4868 -9.76466 -3.76584 -0.45486

 */
void MetodoQRTestSuite::testFrecuencias() {
	CONTADOR filas = 10;
	CONTADOR columnas = 10;
	CONTADOR iteraciones = 200;

	FLOTANTE a[] = {
		-41.48148 , 21.11111, 0, 0, 0, 0, 0, 0, 0, 0,
		20.07042,  -41.37324, 21.30282, 0, 0, 0, 0, 0, 0, 0,
		0,    21.84116, -45.48736, 23.64621, 0, 0, 0, 0, 0, 0,
		0,    0, 21.97987, -38.92617, 16.94631, 0, 0, 0, 0, 0,
		0,    0, 0, 18.23105, -36.28159, 18.05054, 0, 0, 0, 0,
		0,    0, 0, 0, 18.51852, -40.74074, 22.22222, 0, 0, 0,
		0,    0, 0, 0, 0, 21.66065, -45.12635, 23.46570, 0, 0,
		0,    0, 0, 0, 0, 0, 24.07407, -38.88889, 14.81481, 0,
		0,    0, 0, 0, 0, 0, 0, 14.08451, -31.69014, 17.60563,
		0,    0, 0, 0, 0, 0, 0,        0, 18.05054, -18.05054
	};

	MatrizFullFull m(a, filas, columnas);
	MetodoQR qr;
	FLOTANTE *autoValor;
	autoValor = qr.getAutovalores(iteraciones, m);

	FLOTANTE autoValorCorrecto[] = {
			-80.4821, -77.0016, -60.9729, -52.1372, -42.5893,
			-31.1215, -19.5855, -9.88949, -3.81133, -0.455721
	};

	for(CONTADOR i=0; i < filas; i++) {
		TEST_ASSERT_MSG( t.parecido(autoValor[i], autoValorCorrecto[i], ERROR_ACEPTADO),
		                 "Deben ser iguales");
	}

	FLOTANTE frecCorrecta[] = {
			8.97118, 8.77506, 7.80851, 7.22061, 6.52605,
			5.57866, 4.42555, 3.14476, 1.77335, 0.67507
	};

	FLOTANTE *frec;
	frec = qr.getFrecuencias();

	for(CONTADOR i=0; i < filas; i++) {
			TEST_ASSERT_MSG( t.parecido(frec[i], frecCorrecta[i], ERROR_ACEPTADO),
			                 "Deben ser iguales");
	}
}

MetodoQRTestSuite::MetodoQRTestSuite() {
	TEST_ADD(MetodoQRTestSuite::testFrecuencias)
}

MetodoQRTestSuite::~MetodoQRTestSuite() {
}
