/*
 * MoverLavarropasTestSuite.cpp
 *
 *  Created on: Jun 15, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "MoverLavarropasTestSuite.h"

#define ARCHIVO_PRUEBA_10    "data/catedra/prueba10.txt"
#define SALIDA_TEST          "salida.dat"
#define SALIDA_PRECOMPUTE    "data/test/salida.prueba10.precompute.dat"
#define SALIDA_HEURISTICA    "data/test/salida.prueba10.heuristica.dat"

#define ACCESO_LECTURA       "r"
#define TESTING              true

void MoverLavarropasTestSuite::testHeuristica() {
	remove(SALIDA_TEST); // me aseguro que la heurística genere el archivo :-D

	Entrada entrada(ARCHIVO_PRUEBA_10);
	MoverLavarropas ml(&entrada);
	ml.estrategiaAprendizaje(ARCHIVO_PRUEBA_10, TESTING);

    FILE *fpTest;
    fpTest = fopen (SALIDA_TEST, ACCESO_LECTURA);
    TEST_ASSERT_MSG(fpTest != NULL, "No se encuentra el archivo salida.dat");
    FILE *fpTestOK;
    fpTestOK = fopen (SALIDA_HEURISTICA, ACCESO_LECTURA);
    TEST_ASSERT_MSG(fpTestOK != NULL, "No se encuentra el archivo data/test/salida.prueba10.heuristica.dat");
    TEST_ASSERT_MSG(t.sonArchivosIguales(fpTest, fpTestOK), "Los archivos salida.dat y data/test/salida.prueba10.heuristica.dat son distintos");
    fclose(fpTest);
    fclose(fpTestOK);

	remove(SALIDA_TEST);
}

void MoverLavarropasTestSuite::testMetodo2() {
	remove(SALIDA_TEST);
    Entrada entrada(ARCHIVO_PRUEBA_10);
    MoverLavarropas ml(&entrada);
    ml.estrategiaAprendizaje(ARCHIVO_PRUEBA_10, true);

    FILE *fpTest;
    fpTest = fopen (SALIDA_TEST, ACCESO_LECTURA);
    TEST_ASSERT_MSG(fpTest != NULL, "No se encuentra el archivo salida.dat");
    FILE *fpTestOK;
    fpTestOK = fopen (SALIDA_PRECOMPUTE, ACCESO_LECTURA);
    TEST_ASSERT_MSG(fpTestOK != NULL, "No se encuentra el archivo data/test/salida.prueba10.precompute.dat");
    TEST_ASSERT_MSG(t.sonArchivosIguales(fpTest, fpTestOK), "Los archivos salida.dat y data/test/salida.prueba10.precompute.dat son distintos");
    fclose(fpTest);
    fclose(fpTestOK);

    remove(SALIDA_TEST);
}

MoverLavarropasTestSuite::MoverLavarropasTestSuite() {
	TEST_ADD(MoverLavarropasTestSuite::testMetodo2)
	TEST_ADD(MoverLavarropasTestSuite::testHeuristica)
}

MoverLavarropasTestSuite::~MoverLavarropasTestSuite() {

}
