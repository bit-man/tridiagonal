/*
 * TemblorTestSuite.cpp
 *
 *  Created on: Jun 7, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "TemblorTestSuite.h"

#define ARCHIVO_DE_ENTRADA_3  "data/catedra/prueba3.txt"

void TemblorTestSuite::testFrecPeligrosa() {
	Entrada e(ARCHIVO_DE_ENTRADA_3);
	Temblor t(&e);

	TEST_ASSERT_MSG( t.frecuenciaInvalida( FREC_TERREMOTO ),
	                 "3 Hz es una frecuencia peligrosa, y no se detecta como tal");

	TEST_ASSERT_MSG( ! t.frecuenciaInvalida( FREC_MAX + 1 ),
	                 "4.3 Hz NO es una frecuencia peligrosa, y se detecta como si lo fuera");

	TEST_ASSERT_MSG( ! t.frecuenciaInvalida( FREC_MIN - 1 ),
	                 "1.7 Hz NO es una frecuencia peligrosa, y se detecta como si lo fuera");
}

void TemblorTestSuite::testSeCaeTodo() {
	Entrada e(ARCHIVO_DE_ENTRADA_3);
	Temblor t(&e);

	// El archivo de prueba tiene 3 pisos, así que pongo 3 frecuencias
	FLOTANTE frecNoSeCae[] = { 3.4, 2.6, 5 };
	TEST_ASSERT_MSG( ! t.seCaeTodo( frecNoSeCae ),
	                 "Oops, todo se cae y no debería");

	FLOTANTE frecHacePumba[] = { 3.0, 2.6, 5 };
	TEST_ASSERT_MSG( t.seCaeTodo( frecHacePumba ),
	                 "Oops, dice que no pasa nada pero todo se cae");


}
TemblorTestSuite::TemblorTestSuite() {
	TEST_ADD(TemblorTestSuite::testSeCaeTodo)
	TEST_ADD(TemblorTestSuite::testFrecPeligrosa)
}

TemblorTestSuite::~TemblorTestSuite() {
}
