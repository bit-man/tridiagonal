/*
 * TemblorTestSuite.h
 *
 *  Created on: Jun 7, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef TEMBLORTESTSUITE_H_
#define TEMBLORTESTSUITE_H_

#include <cpptest.h>
#include "../tp3/Temblor.h"
#include "../matriz/MatrizFullFull.h"
#include "../io/Entrada.h"
#include "../tp3/Constantes.h"

using namespace std;

class TemblorTestSuite : public Test::Suite {
private:
	void testSeCaeTodo();
	void testFrecPeligrosa();
public:
	TemblorTestSuite();
	virtual ~TemblorTestSuite();
};

#endif /* TEMBLORTESTSUITE_H_ */
