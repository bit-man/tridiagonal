/*
 * MatrizTestSuite.cpp
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "MatrizFullFullTestSuite.h"
#include "../tp3/TiposBasicos.h"

MatrizFullFullTestSuite::MatrizFullFullTestSuite() {
	TEST_ADD(MatrizFullFullTestSuite::gettersAndSetters)
	TEST_ADD(MatrizFullFullTestSuite::igual)
	TEST_ADD(MatrizFullFullTestSuite::redimensionar)
	TEST_ADD(MatrizFullFullTestSuite::testConstrMatriz)
}


void MatrizFullFullTestSuite::testConstrMatriz() {
	CONTADOR filas = 3;
	CONTADOR columnas = 3;

	FLOTANTE  m[] = {
			2, 3, 9 ,
			5, 4, 8 ,
			7, 6, 1
	};

	MatrizFullFull mf( m, filas, columnas);
	for ( CONTADOR i = 0; i < filas; i++ ) {
		for ( CONTADOR j = 0; j < columnas; j++ ){
			TEST_ASSERT_MSG( m[filas*i+j] == mf.get(i,j),"Deben ser iguales");
		}
	}
}

void MatrizFullFullTestSuite::redimensionar() {
	MatrizFullFull a(1,1);

	TEST_ASSERT( a.getFilas() == 1 );
	TEST_ASSERT( a.getColumnas() == 1 );
	TEST_ASSERT( a.get(0,0) == (FLOTANTE) 0 );

	a.set(0,0,10);

	a.redimensionar(2,2);

	TEST_ASSERT( a.getFilas() == 2 );
	TEST_ASSERT( a.getColumnas() == 2 );
	TEST_ASSERT( a.get(0,0) == (FLOTANTE) 10 );
	TEST_ASSERT( a.get(0,1) == (FLOTANTE) 0 );
	TEST_ASSERT( a.get(1,0) == (FLOTANTE) 0 );
	TEST_ASSERT( a.get(1,1) == (FLOTANTE) 0 );


	a.set(0,0,999);
	a.set(0,1,1);
	a.set(1,0,10);
	a.set(1,1,11);

	a.redimensionar(2,1);

	TEST_ASSERT( a.getFilas() == 2 );
	TEST_ASSERT( a.getColumnas() == 1 );
	TEST_ASSERT( a.get(0,0) == (FLOTANTE) 999 );
	TEST_ASSERT( a.get(1,0) == (FLOTANTE) 10 );
}

void MatrizFullFullTestSuite::gettersAndSetters() {
	MatrizFullFull a(2,2);
	a.set(0,0, (FLOTANTE) 1);
	a.set(0,1,(FLOTANTE) 2);
	a.set(1,0,(FLOTANTE) 3);
	a.set(1,1,(FLOTANTE) 4);

	TEST_ASSERT(a.get(0,0) == (FLOTANTE) 1);
	TEST_ASSERT(a.get(0,1) == (FLOTANTE) 2);
	TEST_ASSERT(a.get(1,0) == (FLOTANTE) 3);
	TEST_ASSERT(a.get(1,1) == (FLOTANTE) 4);
}

void MatrizFullFullTestSuite::igual() {
	MatrizFullFull a(1), b(1);
	a.set(0, 9);
	b.set(0, 9);

	TEST_ASSERT_MSG(a.get(0) == b.get(0),"Deben ser iguales");
	TEST_ASSERT_MSG(a == b, "Deben ser iguales (operator==)");
	TEST_ASSERT_MSG(a.esParecidaA(b, (FLOTANTE) 0), "Deben ser parecidas con error = 0");

	FLOTANTE error = 0.1;
	b.set(0, b.get(0) + 2 * error);
	TEST_ASSERT_MSG(a.esParecidaA(b, error) == false, "NO Deben ser parecidas con error = 0");
}



void MatrizFullFullTestSuite::doubleFree() {
	MatrizFullFull a;
	MatrizFullFull *b;

	b = &a;

	MatrizBase base;
	base = a;
}


