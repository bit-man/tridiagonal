/*
 * EntradaTestSuite.cpp
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "EntradaTestSuite.h"

#define ARCHIVO       "EntradaTest.csv"
#define EOL           '\n'

#define COMPARACION_EXACTA				true
#define ERROR_DE_COMPARACION_MAXIMO_TOLERADO	(FLOTANTE) 0.0000001

#define MSG(x)		x.str().c_str()

using namespace std;

EntradaTestSuite::EntradaTestSuite() {
	ofstream archivo(ARCHIVO);
	archivo << "2 3 5" << EOL;
	archivo << "1000 2000" << EOL;
	archivo << "3000 4000" << EOL;
	archivo.close();

	TEST_ADD(EntradaTestSuite::testAbrirArchivo)
	TEST_ADD(EntradaTestSuite::testLeerDatosSimples)
	TEST_ADD(EntradaTestSuite::testLeerMatrices)
	TEST_ADD(EntradaTestSuite::testCatedraPrueba3)
}


void EntradaTestSuite::testAbrirArchivo() {
	Entrada e(ARCHIVO);

	TEST_ASSERT_MSG( e.is_open(), "El archivo debe estar abierto");

	e.close();
}

void EntradaTestSuite::testLeerDatosSimples() {
	Entrada e(ARCHIVO);

	TEST_ASSERT_MSG( e.pisos() == (CONTADOR) 2, "Debe haber 2 pisos");
	TEST_ASSERT_MSG( e.masaLavarropas() == (FLOTANTE) 3, "La masa del lavarropas debe ser 3");
	TEST_ASSERT_MSG( e.masaDelPiso() == (FLOTANTE) 5, "La masa del piso debe ser de 5");

	e.close();
}


void EntradaTestSuite::testLeerMatrices() {
	Entrada e(ARCHIVO);

	CONTADOR nCoef = e.pisos();
	FLOTANTE correcto[] = { (FLOTANTE) 1000, (FLOTANTE) 2000 };
	FLOTANTE *coef = e._coeficientesDeRigidez();
	for(CONTADOR i = 0; i < nCoef; i++) {
		_testElementoDeMatrizCoincideConElEsperado( coef[i], correcto[i],
													i,  1, COMPARACION_EXACTA);
	}


	CONTADOR correctoL[] = {  3000,  4000 };
	CONTADOR *lava = e._lavarropasPorPiso();
	for(CONTADOR i = 0; i < nCoef; i++) {
		_testElementoDeMatrizCoincideConElEsperado(lava[i], correctoL[i], i,  1);
	}

	e.close();
}

void EntradaTestSuite::testCatedraPrueba3() {
	Entrada e("data/catedra/prueba3.txt");

	TEST_ASSERT_MSG( e.is_open(), "El archivo debe estar abierto");

	CONTADOR nPisos = e.pisos();
	TEST_ASSERT_MSG( nPisos == (CONTADOR) 3, "Debe haber 3 pisos");
	TEST_ASSERT_MSG( e.masaLavarropas() == (FLOTANTE) 70, "La masa del lavarropas debe ser 70");
	TEST_ASSERT_MSG( e.masaDelPiso() == (FLOTANTE) 66500, "La masa del piso debe ser de 66500");

	FLOTANTE correcto[] = { (FLOTANTE) 155000, (FLOTANTE) 165000, (FLOTANTE) 140000 };
	FLOTANTE *coef = e._coeficientesDeRigidez();
	for(CONTADOR i = 0; i < nPisos; i++) {
		_testElementoDeMatrizCoincideConElEsperado(coef[i], correcto[i], i, 1, COMPARACION_EXACTA);
	}


	CONTADOR correctoL[] = {  40, 20, 32 };
	CONTADOR *lava = e._lavarropasPorPiso();
	for(CONTADOR i = 0; i < nPisos; i++) {
		_testElementoDeMatrizCoincideConElEsperado(lava[i], correctoL[i], i, 1);
	}
	e.close();
};

EntradaTestSuite::~EntradaTestSuite() {
	remove(ARCHIVO);
}

void EntradaTestSuite::_testElementoDeMatrizCoincideConElEsperado(
		FLOTANTE obtenido, FLOTANTE esperado,
		CONTADOR fila, CONTADOR columna,
		FLAG exacto)
{
	stringstream msg;
	msg << "El elemento de la fila " << fila << ", columna " << columna
	    << " del resultado no coincide con el del original.";
	char c[80];
	sprintf(c, " Esperado : '%+.20Lf'   Obtenido : '%+.20Lf'", esperado, obtenido);
	msg << c;

	FLAG result = exacto ?
						(esperado == obtenido) :
						t.parecido(esperado, obtenido, ERROR_DE_COMPARACION_MAXIMO_TOLERADO );

	TEST_ASSERT_MSG( result, MSG(msg) );
}

void EntradaTestSuite::_testElementoDeMatrizCoincideConElEsperado(
		CONTADOR obtenido, CONTADOR esperado,
		CONTADOR fila, CONTADOR columna )
{
	stringstream msg;
	msg << "El elemento de la fila " << fila << ", columna " << columna
	    << " del resultado no coincide con el del original."
	    <<  " Esperado : '" << esperado << "'   Obtenido : '" << obtenido << "'";

	FLAG result = (esperado == obtenido);

	TEST_ASSERT_MSG( result, MSG(msg) );
}

