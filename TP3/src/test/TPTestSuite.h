/*
 * TPTestSuite.h
 *
 *  Created on: Jun 20, 2009
 *      Author:  Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef TPTESTSUITE_H_
#define TPTESTSUITE_H_

#include <cpptest.h>
#include "../io/Entrada.h"
#include "../matriz/MetodoQR.h"
#include "../tp3/Temblor.h"
#include "../tp3/TiposBasicos.h"

class TPTestSuite : public Test::Suite {
private:
	FLAG _frecsEnOrdenCreciente(const char * nombreDeArchivo);
	void testFrecsEnOrden_PRUEBA3();
	void testFrecsEnOrden_PRUEBA5();
	void testFrecsEnOrden_PRUEBA5A();
	void testFrecsEnOrden_PRUEBA5B();
	void testFrecsEnOrden_PRUEBA5C();
	void testFrecsEnOrden_PRUEBA10();
	void testFrecsEnOrden_PRUEBA10A();
	void testFrecsEnOrden_PRUEBA20();
public:
	TPTestSuite();
	virtual ~TPTestSuite();
};

#endif /* TPTESTSUITE_H_ */
