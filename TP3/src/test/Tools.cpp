/*
 * Tools.cpp
 *
 *  Created on: Jun 7, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "Tools.h"

FLAG Tools::parecido( FLOTANTE x, FLOTANTE y, FLOTANTE errorMax) {
	return abs( (long) errorRelativo(x,y) ) <= errorMax ;
}


FLOTANTE Tools::errorRelativo(FLOTANTE x, FLOTANTE y) {
	FLOTANTE divisor = x == 0 ? y : x;
	return (x - y) / divisor;
}

FLAG Tools::sonArchivosIguales(FILE* fp1, FILE* fp2)
{
    int c, cok;
    FLAG sonDistintos = false;
    do{
        c = fgetc(fp1);
        cok = fgetc(fp2);
        sonDistintos = (c != cok);
    } while((c != EOF) && (cok != EOF) && !sonDistintos);

    return ! sonDistintos;
}

Tools::Tools() {
}

Tools::~Tools() {
}
