/*
 * MoverLavarropasTestSuite.h
 *
 *  Created on: Jun 15, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef MOVERLAVARROPASTESTSUITE_H_
#define MOVERLAVARROPASTESTSUITE_H_

#include <cpptest.h>
#include <cstdio>
#include "../io/Entrada.h"
#include "../tp3/MoverLavarropas.h"
#include "Tools.h"

class MoverLavarropasTestSuite : public Test::Suite  {
private:
	void testMetodo2();
	void testHeuristica();
    void testSonArchivosIguales(const char * archivo1, const char * archivo2);
	Tools t;
public:
	MoverLavarropasTestSuite();
	virtual ~MoverLavarropasTestSuite();
};

#endif /* MOVERLAVARROPASTESTSUITE_H_ */
