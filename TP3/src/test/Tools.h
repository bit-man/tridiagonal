/*
 * Tools.h
 *
 *  Created on: Jun 7, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef TOOLS_H_
#define TOOLS_H_

#include "../tp3/TiposBasicos.h"
#include <cstdio>
#include <cstdlib>

class Tools {
public:
	Tools();
	FLAG parecido( FLOTANTE x, FLOTANTE y, FLOTANTE errorMax);
	FLOTANTE errorRelativo(FLOTANTE x, FLOTANTE y);
	FLAG sonArchivosIguales(FILE* fp1, FILE* fp2);
	virtual ~Tools();
};

#endif /* TOOLS_H_ */
