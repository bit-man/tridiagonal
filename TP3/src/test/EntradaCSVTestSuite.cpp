/*
 * EntradaCSVTestSuite.cpp
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "EntradaCSVTestSuite.h"

#define  ARCHIVO       "EntradaCSVTestSuite.csv"
#define  SEP_CATEDRA   ' '

EntradaCSVTestSuite::EntradaCSVTestSuite() {
	ofstream archivo(ARCHIVO);
	archivo << "12,3.45\n";  // sólo números
	archivo << '"' << "sinEspacios" << '"' << ",6.7\n"; // string sin espacios
	archivo << '"' << "con_comillas" << '\\' << '"' << "al_medio" << '"';
	archivo.close();

	TEST_ADD(EntradaCSVTestSuite::testAdHoc)
	TEST_ADD(EntradaCSVTestSuite::testCatedraPrueba3)
}

void EntradaCSVTestSuite::testAdHoc() {
	EntradaCSV archivo(ARCHIVO);
	TEST_ASSERT_MSG(archivo.is_open(),"No está abierto el archivo EntradaCSVTestSuite.csv");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay más datos en el archivo");

	int entero = archivo.leerEntero();
	TEST_ASSERT_MSG(entero == 12, "Debería encontrar el valor 12");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay más datos en el archivo");

	/***
	 * Hay un problema, al menos en Windows/Cygwin que si a un long double lo comparo
	 * con otro long double aunque sean iguales me da error (o al menos eso pasa con la
	 * operaci�n leerReal)
	 */
	long double real = archivo.leerReal();
	TEST_ASSERT_MSG( real == (float) 3.45, "Debería encontrar el valor 3.45");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"Hay más datos en la línea y no debería");
	TEST_ASSERT_MSG(!archivo.eof(),"No hay más datos en el archivo");

	archivo.siguienteLinea();
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"Hay más datos en la línea y no debería");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay más datos en el archivo");

	string str = archivo.leerCaracteres();
	TEST_ASSERT_MSG( str == "sinEspacios", "Debería encontrar el string 'sinEspacios'");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");
	TEST_ASSERT_MSG(!archivo.eof(),"No hay más datos en el archivo");

	real = archivo.leerReal();
	TEST_ASSERT_MSG( real == (float) 6.7, "Debería encontrar el valor 6.7");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay más datos en el archivo");

	archivo.siguienteLinea();
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"Hay más datos en la línea y no debería");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay más datos en el archivo");

	str = archivo.leerCaracteres();
	TEST_ASSERT_MSG( str == "con_comillas\"al_medio", "Debería encontrar el string 'con_comillas\"al_medio'");
	TEST_ASSERT_MSG(!archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");
	TEST_ASSERT_MSG(archivo.eof(),"No hay más datos en el archivo");

    archivo.siguienteLinea();
	TEST_ASSERT_MSG(archivo.eof(),"No hay más datos en el archivo");

	archivo.close();
}

void EntradaCSVTestSuite::testCatedraPrueba3() {
	EntradaCSV archivo("data/catedra/prueba3.txt");


	TEST_ASSERT_MSG(archivo.is_open(),"No está abierto el archivo  rpueba3.txt");

    archivo.separador(SEP_CATEDRA);

    unsigned int n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 3, "Debería encontrar el valor 3");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 70, "Debería encontrar el valor 70");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 66500, "Debería encontrar el valor 6650");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");


    archivo.siguienteLinea(); // 155000 165000 140000
    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 155000, "Debería encontrar el valor 3155000");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 165000, "Debería encontrar el valor 165000");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 140000, "Debería encontrar el valor 140000");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    archivo.siguienteLinea(); // 40 20 32
    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 40, "Debería encontrar el valor 40");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 20, "Debería encontrar el valor 20");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

    n = archivo.leerEntero();
	TEST_ASSERT_MSG( n == (float) 32, "Debería encontrar el valor 32");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"No hay más datos en la línea y no debería");

	archivo.close();
}

EntradaCSVTestSuite::~EntradaCSVTestSuite() {
	remove(ARCHIVO);
}
