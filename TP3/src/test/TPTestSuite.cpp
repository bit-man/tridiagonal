/*
 * TPTestSuite.cpp
 *
 *  Created on: Jun 20, 2009
 *      Author:  Claudio Gauna, Víctor A. Rodríguez
 */

#include "TPTestSuite.h"

#define ARCHIVO_PRUEBA3       "data/catedra/prueba3.txt"
#define ARCHIVO_PRUEBA5       "data/catedra/prueba5.txt"
#define ARCHIVO_PRUEBA5A      "data/catedra/prueba5a.txt"
#define ARCHIVO_PRUEBA5B      "data/catedra/prueba5b.txt"
#define ARCHIVO_PRUEBA5C      "data/catedra/prueba5c.txt"
#define ARCHIVO_PRUEBA10      "data/catedra/prueba10.txt"
#define ARCHIVO_PRUEBA10A     "data/catedra/prueba10a.txt"
#define ARCHIVO_PRUEBA20      "data/catedra/prueba20.txt"

#define ITERACIONES  200

FLAG TPTestSuite::_frecsEnOrdenCreciente(const char * nombreDeArchivo) {
	MetodoQR qr;
	FLOTANTE * frecuencia;
	Entrada entrada(nombreDeArchivo);
	Temblor temblor(&entrada);

	MatrizFullFull & matriz = temblor.generarMatriz(entrada._lavarropasPorPiso());
	frecuencia = qr.getFrecuencias(ITERACIONES,matriz);

	FLOTANTE ultimaFrec = 99999;
	FLAG ordenCreciente = true;
	for(CONTADOR i=0; i < entrada.pisos() && ordenCreciente; i++ ) {
		ordenCreciente = (frecuencia[i] < ultimaFrec);
		ultimaFrec = frecuencia[i];
	}

	return ordenCreciente;
}

void TPTestSuite::testFrecsEnOrden_PRUEBA3() {
		TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA3) ,
				"Las frecuencias en PRUEBA3 no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA5() {
		TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA5) ,
				"Las frecuencias en PRUEBA35 no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA5A() {
	TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA5A) ,
			"Las frecuencias en PRUEBA5A no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA5B() {
	TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA5B) ,
			"Las frecuencias en PRUEBA5B no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA5C() {
	TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA5C) ,
			"Las frecuencias en PRUEBA5C no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA10() {
	TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA10) ,
			"Las frecuencias en PRUEBA10 no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA10A() {
	TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA10A) ,
			"Las frecuencias en PRUEB10A no están en orden creciente" );
}

void TPTestSuite::testFrecsEnOrden_PRUEBA20() {
	TEST_ASSERT_MSG( _frecsEnOrdenCreciente(ARCHIVO_PRUEBA20) ,
			"Las frecuencias en PRUEBA20 no están en orden creciente" );
}

TPTestSuite::TPTestSuite() {
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA3)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA5)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA5A)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA5B)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA5C)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA10)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA10A)
	TEST_ADD(TPTestSuite::testFrecsEnOrden_PRUEBA20)
}

TPTestSuite::~TPTestSuite() {
}
