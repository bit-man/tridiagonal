/*
 * MetodoQRTestSuite.h
 *
 *  Created on: Jun 6, 2009
 *  Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef METODOQRTESTSUITE_H_
#define METODOQRTESTSUITE_H_

#include <cpptest.h>
#include "Tools.h"
#include "../tp3/TiposBasicos.h"
#include "../matriz/MetodoQR.h"

class MetodoQRTestSuite : public Test::Suite  {
private:
	void testFrecuencias();
	Tools t;

public:
	MetodoQRTestSuite();
	virtual ~MetodoQRTestSuite();
};

#endif /* METODOQRTESTSUITE_H_ */
