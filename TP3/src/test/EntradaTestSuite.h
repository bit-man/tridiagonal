/*
 * EntradaTestSuite.h
 *
 *  Created on: June 3, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef ENTRADATESTSUITE_H_
#define ENTRADATESTSUITE_H_

#include <cpptest.h>
#include "../io/Entrada.h"
#include "../tp3/TiposBasicos.h"
#include "Tools.h"

#include <cstdio>

class EntradaTestSuite  : public Test::Suite {
private:
	Tools t;
    void testAbrirArchivo();
    void testLeerDatosSimples();
    void testLeerMatrices();
    void testCatedraPrueba3();
    void _testElementoDeMatrizCoincideConElEsperado( FLOTANTE obtenido, FLOTANTE esperado,
													 CONTADOR fila, CONTADOR columna,
													 FLAG exacto);
    void _testElementoDeMatrizCoincideConElEsperado( CONTADOR obtenido, CONTADOR esperado,
													 CONTADOR fila, CONTADOR columna );

public:
	EntradaTestSuite();
	~EntradaTestSuite();
};

#endif /* ENTRADATESTSUITE_H_ */
