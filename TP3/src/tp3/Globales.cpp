/*
 * Globales.cpp
 *
 *  Created on: Jun 14, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "Globales.h"


FLAG comparadorAsc(Movimiento m1, Movimiento m2) {
	return (m1.valor < m2.valor);
}

FLAG comparadorDes(Movimiento m1, Movimiento m2) {
	return (m1.valor > m2.valor);
}

FLAG comparadorDiferencias(Diferencia d1, Diferencia d2) {
	return (d1.diferencia > d2.diferencia);
}
