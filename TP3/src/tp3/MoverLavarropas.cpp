/*
 * MoverLavarropas.cpp
 *
 *  Created on: Jun 6, 2009
 *      Author: Claudio Gauna,  Víctor A. Rodríguez
 */

#include "MoverLavarropas.h"

#define ITERACIONES   200

MoverLavarropas::MoverLavarropas(Entrada * e) : movimientosQueSuben(e->pisos()),movimientosQueBajan(e->pisos()) {
	entrada = e;
	entrada->_lavarropasPorPiso();
	Temblor temblor(entrada);
	MatrizFullFull & matriz = temblor.generarMatriz(entrada->_lavarropasPorPiso());
	MetodoQR qr;
	frecuenciasOriginales = qr.getFrecuencias(ITERACIONES,matriz);
	matriz.~MatrizFullFull();
}

MoverLavarropas::~MoverLavarropas() {
	delete[] frecuenciasOriginales;
}

/***
 * Cálculo de movimientos utilizando los lavarropas de a un piso por vez
 */
void MoverLavarropas::precalculo() {
	// Heurística para saber de a cuántos lavarropas mueva en cada piso
	CONTADOR paso;
	if(entrada->pisos() <= 7) {
		paso = 1;
	} else {
		paso = 20;
	}
	Movimiento movimiento;
	Temblor temblor(entrada);
	MetodoQR qr;

	for(CONTADOR i = 0; i < entrada->pisos(); i++) {
		for(CONTADOR j = 0; j < entrada->pisos(); j++) {
			for(CONTADOR k = 1; k*paso < entrada->_lavarropasPorPiso()[i]; k++) {
				if(j != i){
					CONTADOR * lavarropas = entrada->_lavarropasPorPiso();
					lavarropas[i] = lavarropas[i] - k*paso;
					lavarropas[j] = lavarropas[j] + k*paso;
					MatrizFullFull & mat = temblor.generarMatriz(lavarropas);
					FLOTANTE * frecuenciasNuevas = qr.getFrecuencias(200,mat);
					mat.~MatrizFullFull();
					movimiento.cantidad = k*paso;
					movimiento.pisoFuente = i;
					movimiento.pisoDestino = j;
					for(CONTADOR l = 0; l < entrada->pisos();l++) {
						if(frecuenciasOriginales[l] < frecuenciasNuevas[l]) {
							movimiento.valor = frecuenciasNuevas[l];
							movimientosQueSuben[l].push_back(movimiento);
						}
						if(frecuenciasOriginales[l] > frecuenciasNuevas[l]) {
							movimiento.valor = frecuenciasNuevas[l];
							movimientosQueBajan[l].push_back(movimiento);
						}
					}
					//Vuelvo al estado original para la proxima iteracion
					lavarropas[i] = lavarropas[i] + k*paso;
					lavarropas[j] = lavarropas[j] - k*paso;
					delete[] frecuenciasNuevas;
				}
			}
		}
	}
}

FLAG MoverLavarropas::subirFrecuencia(CONTADOR frecuencia,list<Movimiento> & movimientosHechos) {
	FLAG res = false;
	FLAG terminamos = false;
	FLOTANTE ultimoValor = frecuenciasOriginales[frecuencia];
	CONTADOR indice = 0;
	CONTADOR cantidad = 0;
	Temblor temblor(entrada);
	MetodoQR qr;
	while(!terminamos) {
		Movimiento mov = movimientosQueSuben[frecuencia][indice];
		if(temblor.movimientoValido(movimientosHechos,mov)) {
			movimientosHechos.push_back(mov);
			MatrizFullFull & matriz = temblor.moverLavarropas(movimientosHechos);
			FLOTANTE * cambio = qr.getFrecuencias(ITERACIONES,matriz);
			matriz.~MatrizFullFull();
			FLOTANTE frec = cambio[frecuencia];
			if(frec > ultimoValor) {
				if(!temblor.frecuenciaInvalida(frec)){
					res = true;
					terminamos = true;
				}
				ultimoValor = frec;
				cantidad++;
			} else {
				movimientosHechos.pop_back();
			}
			delete[] cambio;
		}
		indice++;
		if(indice == movimientosQueSuben[frecuencia].size()) {
			terminamos = true;
		}
	}
	return res;
}

FLAG MoverLavarropas::bajarFrecuencia(CONTADOR frecuencia, list<Movimiento> & movimientosHechos) {
	FLAG res = false;
	FLAG terminamos = false;
	FLOTANTE ultimoValor = frecuenciasOriginales[frecuencia];
	CONTADOR indice = 0;
	Temblor temblor(entrada);
	MetodoQR qr;

	while(!terminamos) {
		Movimiento mov = movimientosQueBajan[frecuencia][indice];
		if(temblor.movimientoValido(movimientosHechos,mov)) {
			movimientosHechos.push_back(mov);
			MatrizFullFull & matriz = temblor.moverLavarropas(movimientosHechos);
			FLOTANTE * cambio = qr.getFrecuencias(ITERACIONES,matriz);
			matriz.~MatrizFullFull();
			FLOTANTE frec = cambio[frecuencia];
			if(frec < ultimoValor) {
				if(!temblor.frecuenciaInvalida(frec)){
					res = true;
					terminamos = true;
				}
				ultimoValor = frec;
			} else {
				movimientosHechos.pop_back();
			}
			delete[] cambio;
		}
		indice++;
		if(indice == movimientosQueBajan[frecuencia].size()) {
			terminamos = true;
		}
	}
	return res;
}

void MoverLavarropas::bajarFrecuenciaSinEmpeorar(CONTADOR frecuencia,list<Movimiento> & movimientosHechos) {
	FLAG terminamos = false;
	CONTADOR indice = 0;
	Temblor temblor(entrada);
	MetodoQR qr;
	CONTADOR movimientos = temblor.cantidadMovimientos(entrada->_lavarropasPorPiso(),temblor.calcularLavarropas(movimientosHechos));

	while(!terminamos) {
		Movimiento mov = movimientosQueBajan[frecuencia][indice];
		if(temblor.movimientoValido(movimientosHechos,mov)) {
			movimientosHechos.push_back(mov);
			MatrizFullFull & matriz = temblor.moverLavarropas(movimientosHechos);
			FLOTANTE * cambio = qr.getFrecuencias(ITERACIONES,matriz);
			matriz.~MatrizFullFull();
			if(temblor.seCaeTodo(cambio)) {
				movimientosHechos.pop_back();
			} else {
				CONTADOR nuevosMovimientos = temblor.cantidadMovimientos(entrada->_lavarropasPorPiso(),temblor.calcularLavarropas(movimientosHechos));
				if(nuevosMovimientos > movimientos) {
					movimientosHechos.pop_back();
				} else {
					movimientos = nuevosMovimientos;
				}
			}
			delete[] cambio;
		}
		indice++;
		if(indice == movimientosQueBajan[frecuencia].size()) {
			terminamos = true;
		}
	}
}

/***
 * Sube una frecuencia sin empeorar la cantidad de pasos y asegurando
 * que el edificio no se caiga.
 */
void MoverLavarropas::subirFrecuenciaSinEmpeorar(CONTADOR frecuencia,list<Movimiento> & movimientosHechos) {
	FLAG terminamos = false;
	CONTADOR indice = 0;
	Temblor temblor(entrada);
	MetodoQR qr;
	CONTADOR movimientos = temblor.cantidadMovimientos(entrada->_lavarropasPorPiso(),temblor.calcularLavarropas(movimientosHechos));

	while(!terminamos) {
		Movimiento mov = movimientosQueSuben[frecuencia][indice];
		if(temblor.movimientoValido(movimientosHechos,mov)) {
			movimientosHechos.push_back(mov);
			MatrizFullFull & matriz = temblor.moverLavarropas(movimientosHechos);
			FLOTANTE * cambio = qr.getFrecuencias(ITERACIONES,matriz);
			matriz.~MatrizFullFull();
			if(temblor.seCaeTodo(cambio)) {
				movimientosHechos.pop_back();
			} else {
				CONTADOR * lavarropas = temblor.calcularLavarropas(movimientosHechos);
				CONTADOR nuevosMovimientos = temblor.cantidadMovimientos(entrada->_lavarropasPorPiso(),lavarropas);
				delete[] lavarropas;
				if(nuevosMovimientos > movimientos) {
					movimientosHechos.pop_back();
				} else {
					movimientos = nuevosMovimientos;
				}
			}
			delete[] cambio;
		}
		indice++;
		if(indice == movimientosQueSuben[frecuencia].size()) {
			terminamos = true;
		}
	}
}

/***
 * Deja la cantidad mínima de lavarropas que hacen que no se caiga el edificio
 */
void MoverLavarropas::nivelarLavarropas(list<Movimiento> & movimientosHechos) {
	Temblor temblor(entrada);
	MetodoQR qr;
	vector<Diferencia> diferenciasPositivas;
	vector<Diferencia> diferenciasNegativas;

	CONTADOR * lavarropasNuevos = temblor.calcularLavarropas(movimientosHechos);

	Diferencia diferencia;

	for(CONTADOR i = 0; i < entrada->pisos(); i++) {
		if(lavarropasNuevos[i] < entrada->_lavarropasPorPiso()[i]) {
			diferencia.diferencia = entrada->_lavarropasPorPiso()[i] - lavarropasNuevos[i];
			diferencia.indice = i;
			diferenciasNegativas.push_back(diferencia);
		}
		if(lavarropasNuevos[i] > entrada->_lavarropasPorPiso()[i]) {
			diferencia.diferencia = lavarropasNuevos[i] - entrada->_lavarropasPorPiso()[i];
			diferencia.indice = i;
			diferenciasPositivas.push_back(diferencia);
		}
	}

	sort(diferenciasPositivas.begin(),diferenciasPositivas.end(),comparadorDiferencias);
	sort(diferenciasNegativas.begin(),diferenciasNegativas.end(),comparadorDiferencias);

	FLAG mejora = true;
	CONTADOR cantidad = 1;
	CONTADOR ultimoConteo = temblor.cantidadMovimientos(lavarropasNuevos,entrada->_lavarropasPorPiso());
	delete[] lavarropasNuevos;
	for(CONTADOR i = 0; i < diferenciasPositivas.size(); i++) {
		for(CONTADOR j = 0; j < diferenciasNegativas.size(); j++) {
			mejora = true;
			cantidad = 1;
			while(mejora) {
				Movimiento movimiento;
				movimiento.cantidad = cantidad;
				movimiento.pisoFuente = diferenciasPositivas[i].indice;
				movimiento.pisoDestino = diferenciasNegativas[j].indice;
				movimiento.valor = 0;
				movimientosHechos.push_back(movimiento);
				MatrizFullFull & matriz = temblor.moverLavarropas(movimientosHechos);
				FLOTANTE * frecuencias = qr.getFrecuencias(ITERACIONES,matriz);
				matriz.~MatrizFullFull();
				if (temblor.seCaeTodo(frecuencias)) {
					mejora = false;
					movimientosHechos.pop_back();
				} else {
					CONTADOR * lavarropas = temblor.calcularLavarropas(movimientosHechos);
					CONTADOR cantMovimientos = temblor.cantidadMovimientos(lavarropas,entrada->_lavarropasPorPiso());
					delete[] lavarropas;
					if(cantMovimientos < ultimoConteo) {
						ultimoConteo = cantMovimientos;
						cantidad++;
					} else {
						mejora = false;
						movimientosHechos.pop_back();
					}
				}
				delete[] frecuencias;
			}
		}
	}
	subirFrecuenciaSinEmpeorar(entrada->pisos()-1,movimientosHechos);
	subirFrecuenciaSinEmpeorar(entrada->pisos()-2,movimientosHechos);
}

void MoverLavarropas::nivelarLavarropas2(list<Movimiento> & movimientosHechos) {
	Temblor temblor(entrada);
	MetodoQR qr;
	list<Movimiento>::iterator movs = movimientosHechos.end();
	FLAG mejora = true;
	for(CONTADOR i = 1; i < 100; i++) {
		movs--;
		while(movs != movimientosHechos.end()) {
			while(mejora) {
				movs->cantidad = movs->cantidad - 1;
				FLOTANTE * frecuencias = qr.getFrecuencias(ITERACIONES,temblor.moverLavarropas(movimientosHechos));
				if(temblor.seCaeTodo(frecuencias)) {
					mejora = false;
					movs->cantidad = movs->cantidad + 1;
				}
				delete[] frecuencias;
			}
			movs--;
		}
	}
	subirFrecuenciaSinEmpeorar(entrada->pisos()-1,movimientosHechos);
	subirFrecuenciaSinEmpeorar(entrada->pisos()-2,movimientosHechos);
}

void MoverLavarropas::estrategiaAprendizaje(char * archivoDeEntrada,FLAG testing) {
	Temblor temblor(entrada);
	MetodoQR qr;
	MatrizFullFull  matriz;
	list<Movimiento> movimientosBuenos,movimientos,movimientosHechos,movimientosNiveladores;
	list<CONTADOR> frecuenciasParaCorregir;

	for(CONTADOR i = 0; i < entrada->pisos(); i++) {
		if(temblor.frecuenciaInvalida(frecuenciasOriginales[i])) {
			frecuenciasParaCorregir.push_back(i);
		}
	}

	if(frecuenciasParaCorregir.size() == 0) {
		cout << endl << "Esta estable" << endl;
		return;
	}

	precalculo();

	//Programar heuristica
	//Ordeno los movimientos deacuerdo a lo que suben o bajan
	vector<Movimiento>::iterator inicio,final;
	for(CONTADOR i = 0; i < entrada->pisos(); i++) {
		inicio = movimientosQueSuben[i].begin();
		final = movimientosQueSuben[i].end();
		sort(inicio,final,comparadorDes);
		inicio = movimientosQueBajan[i].begin();
		final = movimientosQueBajan[i].end();
		sort(inicio,final,comparadorAsc);
	}

	list<CONTADOR>::iterator pos;
	pos = find(frecuenciasParaCorregir.begin(),frecuenciasParaCorregir.end(),0);
	FLAG subir = true;
	if (pos != frecuenciasParaCorregir.end()) {
		subir = false;
	}

	CONTADOR frecuencia;

	if(subir) {
		frecuencia = frecuenciasParaCorregir.back();
		FLAG resultadoSubir = subirFrecuencia(frecuencia,movimientosHechos);
		if(!resultadoSubir) {
			frecuencia = frecuenciasParaCorregir.front();
			movimientosHechos.clear();
			FLAG resultadoBajar = bajarFrecuencia(frecuencia,movimientosHechos);
			if(!resultadoBajar) {
				cout << "NO se encontro una solucion" << endl;
				return;
			}
		}
	} else {
		frecuencia = frecuenciasParaCorregir.front();
		FLAG resultadoBajar = bajarFrecuencia(frecuencia,movimientosHechos);
		if(!resultadoBajar) {
			FLAG resultadoSubir = subirFrecuencia(frecuencia,movimientosHechos);
			if(!resultadoSubir) {
				cout << "NO se encontro una solucion" << endl;
				return;
			}
		}
	}
	nivelarLavarropas2(movimientosHechos);
	mostrarMovimientosHechos(movimientosHechos,testing);
}

void MoverLavarropas::mostrarMovimientosHechos(list<Movimiento> m, FLAG testing) {
	if (!testing) {
		Temblor temblor(entrada);
		CONTADOR * nuevosPisos = temblor.calcularLavarropas(m);
		CONTADOR cantidadMovimientos = temblor.cantidadMovimientos(nuevosPisos,entrada->_lavarropasPorPiso());
		cout << endl << "Cantidad de movimientos: " << cantidadMovimientos << endl;
		cout << endl << "Configuracion final: [";
		for(CONTADOR i = 0; i < entrada->pisos(); i++) {
			cout << nuevosPisos[i] << " ";
		}
		cout << "]" << endl << endl;
		delete[] nuevosPisos;
	}
}
