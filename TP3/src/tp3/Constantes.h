/*
 * Constantes.h
 *
 *  Created on: Jun 7, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef CONSTANTES_H_
#define CONSTANTES_H_

#define FREC_MAX      		3.3
#define FREC_MIN      		2.7
#define FREC_TERREMOTO   	3

#define EN_ZONA_PELIGROSA(x) 	( (x) >= FREC_MIN) && ( (x) <= FREC_MAX)

#endif /* CONSTANTES_H_ */
