/*
 * Temblor.h
 *
 *  Created on: 06/06/2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef TEMBLOR_H_
#define TEMBLOR_H_
#include "../matriz/MatrizFullFull.h"
#include "../io/Entrada.h"
#include "Constantes.h"
#include <list>
#include <vector>



class Temblor {
	Entrada * entrada;
public:
	Temblor(Entrada * e);
	virtual ~Temblor();
	MatrizFullFull & moverLavarropas(list<Movimiento> & movimientos);
	MatrizFullFull & moverLavarropas(vector<Movimiento> & movimientos);

	/**
	 * Verifica que las frecuencias calculadas no caigan dentro del
	 * rango de FREC_MIN y FREC_MAX
	 */
	FLAG seCaeTodo( FLOTANTE * frecuencia );

	// Id. anterior pero para una sola frecuencia
	FLAG frecuenciaPeligrosa(FLOTANTE frecuencia);

	/***
	 * Genera la matriz correspondiente al sistema de ecuaciones
	 * diferenciales que nos permiten resolver las frecuencias
	 * naturales del sistema, que indican la estabilidad del
	 * edificio durante un terremoto
	 */
	MatrizFullFull & generarMatriz(CONTADOR * lavarropas);
	FLAG frecuenciaInvalida(FLOTANTE frecuencia);
	FLAG movimientoValido(list<Movimiento> &,Movimiento);
	CONTADOR * calcularLavarropas(list<Movimiento> & lista);
	CONTADOR cantidadMovimientos(CONTADOR * pisosNuevos, CONTADOR * pisosViejos);
};

#endif /* TEMBLOR_H_ */
