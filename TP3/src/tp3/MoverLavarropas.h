/*
 * MoverLavarropas.h
 *
 *  Created on: Jun 6, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef MOVERLAVARROPAS_H_
#define MOVERLAVARROPAS_H_


#include <list>
#include <algorithm>

#include "TiposBasicos.h"
#include "../io/Entrada.h"
#include "Constantes.h"
#include "../matriz/MetodoQR.h"
#include "Temblor.h"
#include "Globales.h"

using namespace std;

/***
 * Clase para el cálculo de cuáles lavarropas mover.
 * Se asume que cuando se llama a una estrategia es porque
 * el edificio está en peligro, indicado porque uno de los
 * pisos tiene su frecuencia de resonancia dentro de la
 * franja de peligro
 */
class MoverLavarropas {
private:
	Entrada * entrada;
	void precalculo();
	vector<vector<Movimiento> > movimientosQueSuben;
	vector<vector<Movimiento> > movimientosQueBajan;
	FLOTANTE * frecuenciasOriginales;
	list<Movimiento> movimientosHechos;
public:
	MoverLavarropas( Entrada * e);
	virtual ~MoverLavarropas();
	FLAG subirFrecuencia(CONTADOR frecuencia,list<Movimiento> & movimientosHechos);
	FLAG bajarFrecuencia(CONTADOR frecuencia,list<Movimiento> & movimientosHechos);
	void estrategiaAprendizaje(char * archivoDeEntrada,FLAG testing);
	void bajarFrecuenciaSinEmpeorar(CONTADOR frecuencia,list<Movimiento> & movimientosHechos);
	void subirFrecuenciaSinEmpeorar(CONTADOR frecuencia,list<Movimiento> & movimientosHechos);
	void nivelarLavarropas(list<Movimiento> & movimientosHechos);
	void nivelarLavarropas2(list<Movimiento> & movimientosHechos);
	void mostrarMovimientosHechos(list<Movimiento> m, FLAG testing);
};

#endif /* MOVERLAVARROPAS_H_ */
