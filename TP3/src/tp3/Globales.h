/*
 * Globales.h
 *
 *  Created on: Jun 14, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef GLOBALES_H_
#define GLOBALES_H_

#include "TiposBasicos.h"

FLAG comparadorAsc(Movimiento m1, Movimiento m2);
FLAG comparadorDes(Movimiento m1, Movimiento m2);
FLAG comparadorDiferencias(Diferencia d1, Diferencia d2);

#endif /* GLOBALES_H_ */
