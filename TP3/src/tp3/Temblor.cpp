/*
 * Temblor.cpp
 *
 *  Created on: 06/06/2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "Temblor.h"
#include "TiposBasicos.h"
#include "Constantes.h"

Temblor::Temblor(Entrada * e) {
	entrada = e;
}

Temblor::~Temblor() {
}

MatrizFullFull & Temblor::moverLavarropas(list<Movimiento> & movimientos) {
	CONTADOR * lavarropas = entrada->_lavarropasPorPiso();
	list<Movimiento>::iterator it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] - it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] + it->cantidad;
		it++;
	}
	MatrizFullFull & mat = generarMatriz(lavarropas);

	//Vuelvo al estado anterior
	it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] + it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] - it->cantidad;
		it++;
	}
	return mat;
}

MatrizFullFull & Temblor::moverLavarropas(vector<Movimiento> & movimientos) {
	CONTADOR * lavarropas = entrada->_lavarropasPorPiso();
	vector<Movimiento>::iterator it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] - it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] + it->cantidad;
		it++;
	}
	MatrizFullFull & mat = generarMatriz(lavarropas);

	//Vuelvo al estado anterior
	it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] + it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] - it->cantidad;
		it++;
	}
	return mat;
}

MatrizFullFull & Temblor::generarMatriz(CONTADOR * lavarropas) {
	MatrizFullFull * matriz = new MatrizFullFull(entrada->pisos(),entrada->pisos());
	CONTADOR n = entrada->pisos();
	//seteo la fila 0
	FLOTANTE masaPiso = entrada->masaDelPiso() + (entrada->masaLavarropas() * (FLOTANTE)(lavarropas[0]));
	FLOTANTE valor = (-entrada->_coeficientesDeRigidez()[0] - entrada->_coeficientesDeRigidez()[1] ) / masaPiso;
	matriz->set(0,0,valor);
	valor = entrada->_coeficientesDeRigidez()[1] / masaPiso;
	matriz->set(0,1,valor);

	//Seteo las filas 1 to n-2
	for(CONTADOR i = 1; i < n-1; i++) {
		masaPiso = entrada->masaDelPiso() + (entrada->masaLavarropas() * (FLOTANTE)(lavarropas[i]));
		valor = entrada->_coeficientesDeRigidez()[i] / masaPiso;
		matriz->set(i,i-1,valor);
		valor = (-entrada->_coeficientesDeRigidez()[i]-entrada->_coeficientesDeRigidez()[i+1]) / masaPiso;
		matriz->set(i,i,valor);
		valor = entrada->_coeficientesDeRigidez()[i+1] / masaPiso;
		matriz->set(i,i+1,valor);
	}

	//seteo la fila n-1
	masaPiso = entrada->masaDelPiso() + (entrada->masaLavarropas() * (FLOTANTE)(lavarropas[n-1]));
	valor = entrada->_coeficientesDeRigidez()[n-1] / masaPiso;
	matriz->set(n-1,n-2,valor);
	valor = -entrada->_coeficientesDeRigidez()[n-1] / masaPiso;
	matriz->set(n-1,n-1,valor);

	return *matriz;
}

FLAG Temblor::seCaeTodo( FLOTANTE * frecuencia) {
	FLAG seCae = false;

	for( CONTADOR i = 0; i < entrada->pisos() && ! seCae ; i++ ) {
		seCae = EN_ZONA_PELIGROSA(frecuencia[i]);
	}

	return seCae;
}

FLAG Temblor::frecuenciaInvalida(FLOTANTE frecuencia) {
	return (frecuencia > FREC_MIN) && (frecuencia < FREC_MAX);
}

FLAG Temblor::frecuenciaPeligrosa(FLOTANTE frecuencia) {
	return EN_ZONA_PELIGROSA(frecuencia);
}

FLAG Temblor::movimientoValido(list<Movimiento> & movimientos, Movimiento movimiento) {
	CONTADOR * lavarropas = entrada->_lavarropasPorPiso();
	list<Movimiento>::iterator it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] - it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] + it->cantidad;
		it++;
	}

	FLAG valido = (lavarropas[movimiento.pisoFuente] >= movimiento.cantidad);

	//Vuelvo al estado anterior
	it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] + it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] - it->cantidad;
		it++;
	}

	return valido;
}

CONTADOR * Temblor::calcularLavarropas(list<Movimiento> & movimientos) {
	CONTADOR * lavarropas = new CONTADOR[entrada->pisos()];
	for(CONTADOR i = 0; i < entrada->pisos(); i++) {
		lavarropas[i] = entrada->_lavarropasPorPiso()[i];
	}
	list<Movimiento>::iterator it = movimientos.begin();
	while(it != movimientos.end()) {
		lavarropas[it->pisoFuente] = lavarropas[it->pisoFuente] - it->cantidad;
		lavarropas[it->pisoDestino] = lavarropas[it->pisoDestino] + it->cantidad;
		it++;
	}
	return lavarropas;
}

CONTADOR Temblor::cantidadMovimientos(CONTADOR * pisosNuevos, CONTADOR * pisosViejos) {
	CONTADOR total = 0;
	for(CONTADOR i = 0; i < entrada->pisos(); i++) {
		if(pisosNuevos[i] < pisosViejos[i]) {
			total += (pisosViejos[i] - pisosNuevos[i]);
		} else {
			total += (pisosNuevos[i] - pisosViejos[i]);
		}
	}
	return total / 2;
}
