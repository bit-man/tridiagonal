/*
 * TiposBasicos.h
 *
 *  Created on: June 3, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#ifndef TIPOSBASICOS_H_
#define TIPOSBASICOS_H_

#define ENTERO          int
#define FLOTANTE        long double
#define CONTADOR        unsigned int
#define NADA            void
#define FLAG            bool

typedef struct {
	CONTADOR cantidad;
	CONTADOR pisoFuente;
	CONTADOR pisoDestino;
	FLOTANTE valor;
} Movimiento;

typedef struct {
	CONTADOR iMenor;
	CONTADOR iMayor;
} Indices;

typedef struct {
	CONTADOR indice;
	CONTADOR diferencia;
}Diferencia;

#endif /* TIPOSBASICOS_H_ */

