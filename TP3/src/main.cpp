/*
 * main.cpp
 *
 *  Created on: May 1, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "tp3/MoverLavarropas.h"


using namespace std;

int main(int argc, char* argv[]) {
	Entrada entrada(argv[1]);
	MoverLavarropas ml(&entrada);
	ml.estrategiaAprendizaje(argv[1],false);
}
