## Lagrange testing

1;

# Un t�rmin de Lagrange para una funci�n de R en R

function retval = L2(x)
     x0 = 1
     x1 = 2
     x2 = 3
     retval = ( x - x0 ) * ( x - x2 ) / (  (x1 - x0) * (x1 - x2) )
endfunction

function retval = L22(x,y)
     x0 = 1
     x1 = 2
     x2 = 3
     y0 = 1
     y1 = 2
     y2 = 3
     retval = ( x - x0 ) * ( x - x2 ) / ( 2 * (x1 - x0) * (x1 - x2) ) + ( y - y0 ) * ( y - y2 ) / ( 2 * (y1 - y0) * (y1 - y2) ) 
endfunction

