/*
 * MatrizFullFullTestSuite.h
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef MATRIZFULLFULLTESTSUITE_H_
#define MATRIZFULLFULLTESTSUITE_H_

#include <cpptest.h>
#include "../matriz/MatrizFullFull.h"
#include <iostream>

using namespace std;

class MatrizFullFullTestSuite : public Test::Suite {

private:
	void gettersAndSetters();
	void igual();
	void redimensionar();
	void doubleFree();
public:
	MatrizFullFullTestSuite();
};

#endif /* MATRIZFULLFULLTESTSUITE_H_ */
