/*
 * EstamosEnElHornoTest.h
 *
 *  Created on: May 13, 2009
 *      Author: bit-man
 */

#ifndef ESTAMOSENELHORNOTESTSUITE_H_
#define ESTAMOSENELHORNOTESTSUITE_H_

#include <cpptest.h>
#include "../tp2/EstamosEnElHorno.h"
#include "../matriz/MatrizFullFull.h"

class EstamosEnElHornoTestSuite : public Test::Suite {
public:
	EstamosEnElHornoTestSuite();

private:
	void simpleTest();
	void cuatroSensores();
};

#endif /* ESTAMOSENELHORNOTESTSUITE_H_ */
