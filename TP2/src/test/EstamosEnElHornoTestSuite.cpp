/*
 * EstamosEnElHornoTest.cpp
 *
 *  Created on: May 13, 2009
 *      Author: bit-man
 */

#include "EstamosEnElHornoTestSuite.h"

#define TEMP_INTERNA     (FLOTANTE)  1500

EstamosEnElHornoTestSuite::EstamosEnElHornoTestSuite() {
	TEST_ADD(EstamosEnElHornoTestSuite::simpleTest)
	TEST_ADD(EstamosEnElHornoTestSuite::cuatroSensores)

}

void EstamosEnElHornoTestSuite::simpleTest() {
	EstamosEnElHorno h;
	MatrizFullFull tempExt(1);

	tempExt.set(0,TEMP_INTERNA);
	h.tempInterna(TEMP_INTERNA);
	h.tempExterna( tempExt );
	h.radioExterno( (FLOTANTE) 2 );
	h.radioInterno( (FLOTANTE) 1 );
	h.cantMuestrasRadio( 1 );
	h.calcularTemperaturas();

	MatrizFullFull result;
	h._fase1();
	h._fase2();
	result = (MatrizFullFull &) h.result();
//	result.mostrar();
	cout << endl << "----------------------------------------" << endl;
	TEST_ASSERT_MSG( result.get(0,0) == TEMP_INTERNA, "La temperatura no es de 1500 grados Celsius");
}

void EstamosEnElHornoTestSuite::cuatroSensores() {
	EstamosEnElHorno h;
	MatrizFullFull tempExt(4);

	tempExt.set(0,TEMP_INTERNA);
	tempExt.set(1,TEMP_INTERNA);
	tempExt.set(2,TEMP_INTERNA);
	tempExt.set(3,TEMP_INTERNA);

	h.tempInterna(TEMP_INTERNA);
	h.tempExterna( tempExt );
	h.radioExterno( (FLOTANTE) 2 );
	h.radioInterno( (FLOTANTE) 1 );
	h.cantMuestrasRadio( 4 );
	h.calcularTemperaturas();

	MatrizFullFull result;
	h._fase1();
	h._fase2();
	result = (MatrizFullFull &) h.result();
//	result.mostrar();
	TEST_ASSERT_MSG( result.get(0,0) == TEMP_INTERNA, "La temperatura no es de 1500 grados Celsius");
	TEST_ASSERT_MSG( result.get(0,1) == TEMP_INTERNA, "La temperatura no es de 1500 grados Celsius");
	TEST_ASSERT_MSG( result.get(1,0) == TEMP_INTERNA, "La temperatura no es de 1500 grados Celsius");
	TEST_ASSERT_MSG( result.get(1,1) == TEMP_INTERNA, "La temperatura no es de 1500 grados Celsius");

}
