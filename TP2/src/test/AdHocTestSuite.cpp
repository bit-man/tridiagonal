/*
 * GaussTestSuite.cpp
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include "AdHocTestSuite.h"

#define NUMERO_REPR_TRUNCADA      		3.45
#define NUMERO_FLOAT_REPR_TRUNCADA     	3.45f
#define NUMERO_REPR_EXACTA      		0.5
#define NUMERO_FLOAT_REPR_EXACTA     	0.5f

AdHocTestSuite::AdHocTestSuite() {
	TEST_ADD(AdHocTestSuite::floatVsLongDoubleInexacto)
	TEST_ADD(AdHocTestSuite::floatVsLongDoubleExacto)
}

/***
 * Este test sirve para mostrar algo que, parece, es bastante sabido en el mundo C/C++
 * Cuando comparo dos números, si estos son de distinta precisión y su representación no
 * concide con el número original (se trunca), entonces no comparan.
 */
void AdHocTestSuite::floatVsLongDoubleInexacto() {
    long double lDouble = (long double) NUMERO_REPR_TRUNCADA;
    float lFloat = NUMERO_REPR_TRUNCADA;
    long double lDoubleConCast = lFloat;
    long double lDoubleDirecto = NUMERO_FLOAT_REPR_TRUNCADA;


    TEST_ASSERT_MSG( lDoubleConCast == lDouble,
    		"No puedo comparar un número con cast a long double y long double que viene de un float");
    TEST_ASSERT_MSG( lDoubleDirecto == lDouble,
    		"No puedo comparar un long double grabado como float y long double que viene de un float");
    TEST_ASSERT_MSG( lDoubleDirecto == lFloat,
    		"No puedo comparar un long double grabado como float y  un float");

}

void AdHocTestSuite::floatVsLongDoubleExacto() {
    long double lDouble = (long double) NUMERO_REPR_EXACTA;
    float lFloat = NUMERO_REPR_EXACTA;
    long double lDoubleConCast = lFloat;
    long double lDoubleDirecto = NUMERO_FLOAT_REPR_EXACTA;
    TEST_ASSERT_MSG( lDoubleConCast == lDouble,
    		"No puedo comparar un número con cast a long double y long double que viene de un float");
    TEST_ASSERT_MSG( lDoubleDirecto == lDouble,
    		"No puedo comparar un long double grabado como float y long double que viene de un float");
    TEST_ASSERT_MSG( lDoubleDirecto == lFloat,
    		"No puedo comparar un long double grabado como float y  un float");

}

