/*
 * GaussTestSuite.h
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef GAUSSTESTSUITE_H_
#define GAUSSTESTSUITE_H_

#include <cpptest.h>
#include "../matriz/Gauss.h"
#include <iostream>

using namespace std;

class GaussTestSuite : public Test::Suite {
private:
	void unaIncognita();
	void dosIncognitas();
	void cuatroIncognitas();

public:
	GaussTestSuite();
};

#endif /* GAUSSTESTSUITE_H_ */
