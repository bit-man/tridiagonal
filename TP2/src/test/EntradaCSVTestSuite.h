/*
 * EntradaCSVTestSuite.h
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef ENTRADACSVTESTSUITE_H_
#define ENTRADACSVTESTSUITE_H_

#include <cpptest.h>
#include "../io/EntradaCSV.h"

#include <cstdio>

class EntradaCSVTestSuite  : public Test::Suite {
private:
	void crearArchivo();
	void abrirArchivo();
public:
	EntradaCSVTestSuite();
	~EntradaCSVTestSuite();
};

#endif /* ENTRADACSVTESTSUITE_H_ */
