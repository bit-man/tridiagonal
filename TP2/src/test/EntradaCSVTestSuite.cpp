/*
 * EntradaCSVTestSuite.cpp
 *
 *  Created on: May 2, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include "EntradaCSVTestSuite.h"

#define  ARCHIVO   "EntradaCSVTestSuite.csv"

EntradaCSVTestSuite::EntradaCSVTestSuite() {
	ofstream archivo(ARCHIVO);
	archivo << "12,3.45\n";  // s�lo n�meros
	archivo << '"' << "sinEspacios" << '"' << ",6.7\n"; // string sin espacios
	archivo << '"' << "con_comillas" << '\\' << '"' << "al_medio" << '"';
	archivo.close();

	TEST_ADD(EntradaCSVTestSuite::abrirArchivo)
}

void EntradaCSVTestSuite::abrirArchivo() {
	EntradaCSV archivo(ARCHIVO);
	TEST_ASSERT_MSG(archivo.is_open(),"No est� abierto el archivo EntradaCSVTestSuite.csv");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay m�s datos en la l�nea");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay m�s datos en el archivo");

	int entero = archivo.leerEntero();
	TEST_ASSERT_MSG(entero == 12, "Deber�a encontrar el valor 12");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay m�s datos en la l�nea");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay m�s datos en el archivo");

	/***
	 * Hay un problema, al menos en Windows/Cygwin que si a un long double lo comparo
	 * con otro long double aunque sean iguales me da error (o al menos eso pasa con la
	 * operaci�n leerReal)
	 */
	long double real = archivo.leerReal();
	TEST_ASSERT_MSG( real == (float) 3.45, "Deber�a encontrar el valor 3.45");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"Hay m�s datos en la l�nea y no deber�a");
	TEST_ASSERT_MSG(!archivo.eof(),"No hay m�s datos en el archivo");

	archivo.siguienteLinea();
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"Hay m�s datos en la l�nea y no deber�a");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay m�s datos en el archivo");

	string str = archivo.leerCaracteres();
	TEST_ASSERT_MSG( str == "sinEspacios", "Deber�a encontrar el string 'sinEspacios'");
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"No hay m�s datos en la l�nea y no deber�a");
	TEST_ASSERT_MSG(!archivo.eof(),"No hay m�s datos en el archivo");

	real = archivo.leerReal();
	TEST_ASSERT_MSG( real == (float) 6.7, "Deber�a encontrar el valor 6.7");
	TEST_ASSERT_MSG(! archivo.hayMasDatosEnEstaLinea(),"No hay m�s datos en la l�nea y no deber�a");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay m�s datos en el archivo");

	archivo.siguienteLinea();
	TEST_ASSERT_MSG(archivo.hayMasDatosEnEstaLinea(),"Hay m�s datos en la l�nea y no deber�a");
	TEST_ASSERT_MSG(! archivo.eof(),"No hay m�s datos en el archivo");

	str = archivo.leerCaracteres();
	TEST_ASSERT_MSG( str == "con_comillas\"al_medio", "Deber�a encontrar el string 'con_comillas\"al_medio'");
	TEST_ASSERT_MSG(!archivo.hayMasDatosEnEstaLinea(),"No hay m�s datos en la l�nea y no deber�a");
	TEST_ASSERT_MSG(!archivo.eof(),"No hay m�s datos en el archivo");
    
    archivo.siguienteLinea();
	TEST_ASSERT_MSG(archivo.eof(),"No hay m�s datos en el archivo");

	archivo.close();
}


EntradaCSVTestSuite::~EntradaCSVTestSuite() {
	remove(ARCHIVO);
}
