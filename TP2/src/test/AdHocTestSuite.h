/*
 * GaussTestSuite.h
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef ADHOCTESTSUITE_H_
#define ADHOCTESTSUITE_H_

#include <cpptest.h>

using namespace std;

class AdHocTestSuite : public Test::Suite {
private:
	void floatVsLongDoubleInexacto();
	void floatVsLongDoubleExacto();

public:
	AdHocTestSuite();
};

#endif /* ADHOCTESTSUITE_H_ */
