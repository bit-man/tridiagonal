/*
 * EntradaTestSuite.cpp
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include "EntradaTestSuite.h"

#define ARCHIVO       "EntradaTest.csv"
#define EOL           '\n'

EntradaTestSuite::EntradaTestSuite() {
	ofstream archivo(ARCHIVO);
	archivo << "1500,10,50,100" << EOL;
	archivo << "8.3,2.567,3.666,4.14" << EOL;
	archivo.close();

	TEST_ADD(EntradaTestSuite::abrirArchivo)
	TEST_ADD(EntradaTestSuite::leerDatosSimples)
	TEST_ADD(EntradaTestSuite::leerMatriz)
}


void EntradaTestSuite::abrirArchivo() {
	Entrada e(ARCHIVO);

	TEST_ASSERT_MSG( e.is_open(), "El archivo debe estar abierto");

	e.close();
}

void EntradaTestSuite::leerDatosSimples() {
	Entrada e(ARCHIVO);

	TEST_ASSERT_MSG( e.cantMuestrasRadio() == (FLOTANTE) 100, "Debe haber 100 muestras");
	TEST_ASSERT_MSG( e.radioExterno() == (FLOTANTE) 50, "El radio interno debe ser de 50");
	TEST_ASSERT_MSG( e.radioInterno() == (FLOTANTE) 10, "El radio interno debe ser de 10");
	TEST_ASSERT_MSG( e.tempInterna() == (FLOTANTE) 1500, "Debe haber 1500 grados Celcius");

	e.close();
}

void EntradaTestSuite::leerMatriz() {
	Entrada e(ARCHIVO);

	FLOTANTE correcto[] = { (float) 8.3, (float)2.567, (float)3.666, (float)4.14};
	for(unsigned int i = 0; i < e.tempExterna().getFilas(); i++) {
		TEST_ASSERT_MSG( e.tempExterna().get(i) == correcto[i],"No esperaba este dato");
	}

	e.close();
}

EntradaTestSuite::~EntradaTestSuite() {
	remove(ARCHIVO);
}
