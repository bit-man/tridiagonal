/*
 * EntradaTestSuite.h
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef ENTRADATESTSUITE_H_
#define ENTRADATESTSUITE_H_

#include <cpptest.h>
#include "../io/Entrada.h"


class EntradaTestSuite : public Test::Suite {
public:
	EntradaTestSuite();
	void abrirArchivo();
	void leerDatosSimples();
	void leerMatriz();
	virtual ~EntradaTestSuite();
};

#endif /* ENTRADATESTSUITE_H_ */
