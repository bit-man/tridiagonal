/*
 * GaussTestSuite.cpp
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include "GaussTestSuite.h"

GaussTestSuite::GaussTestSuite() {
	TEST_ADD(GaussTestSuite::unaIncognita)
	TEST_ADD(GaussTestSuite::dosIncognitas)
	TEST_ADD(GaussTestSuite::cuatroIncognitas)
}


void GaussTestSuite::unaIncognita() {
	MatrizFullFull a(1,2);
	a.set(0,0, (FLOTANTE) 2);
	a.set(0,1,(FLOTANTE) 2);

    Gauss g;
    g.resolverSistema(a);

	TEST_ASSERT_MSG(g.result().get(0,0) == (FLOTANTE) 1,
					"La soluci�n de 2x = 2 debe ser x = 1");

}

/***
 *   A = 3  1      B = 7
 *       2  5          9
 *
 *    x =  [2 1]
 ***/
void GaussTestSuite::dosIncognitas() {
	unsigned int incognitas = 2;
	MatrizFullFull a(incognitas,incognitas+1);
	a.set(0,0,3);
	a.set(0,1,1);
	a.set(0,2,7);
	a.set(1,0,2);
	a.set(1,1,5);
	a.set(1,2,9);

	MatrizFullFull correcto(incognitas);
	correcto.set(0, (FLOTANTE) 2);
	correcto.set(1, (FLOTANTE) 1);

    Gauss g;
    g.resolverSistema(a);

	FLOTANTE error = 0.0005;
	TEST_ASSERT( correcto.esParecidaA(g.result(), error) );

}

/***
 *   A = 7 -2 1  2     B = 3
 *       2  8 3  1        -2
 *       -1  0 5  2        5
 *       0  2 -1 4         4
 *
 *    x =  [-0.2  -0.5 0.4 1.4]
 ***/

void GaussTestSuite::cuatroIncognitas() {
	unsigned int incognitas = 4;
	MatrizFullFull a(incognitas,incognitas+1);
	a.set(0,0,7);
	a.set(0,1,-2);
	a.set(0,2,1);
	a.set(0,3,2);
	a.set(0,4,3);
	a.set(1,0,2);
	a.set(1,1,8);
	a.set(1,2,3);
	a.set(1,3,1);
	a.set(1,4,-2);
	a.set(2,0,-1);
	a.set(2,1,0);
	a.set(2,2,5);
	a.set(2,3,2);
	a.set(2,4,5);
	a.set(3,0,0);
	a.set(3,1,2);
	a.set(3,2,-1);
	a.set(3,3,4);
	a.set(3,4,4);

	MatrizFullFull correcto(incognitas);
	correcto.set(0, (FLOTANTE) -0.2);
	correcto.set(1, (FLOTANTE) -0.5);
	correcto.set(2, (FLOTANTE) 0.4);
	correcto.set(3, (FLOTANTE) 1.4);

    Gauss g;
    g.resolverSistema(a);

	FLOTANTE error = 0.2;
	TEST_ASSERT(correcto.esParecidaA( g.result(), error) );

}
