/*
 * main.cpp
 *
 *  Created on: May 1, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include <iostream>
#include "io/Entrada.h"
#include "tp2/EstamosEnElHorno.h"

#define TEMP_INTERNA                 1500

using namespace std;

int main(int argc, char* argv[]) {
//	MatrizFullFull a(1,1);
//	a.set(0,0,100);
//	a.mostrar();
//	cout << "get() " << a.get(0,0) << endl;
	if (argc != 2) {
        cout << endl << "Falta el nombre del archivo de entrada" << endl;
        return -1;
    }

    const char* nomArchivo = argv[1];
    cout << endl << "Archivo de entrada : " << nomArchivo << endl;

    Entrada e(nomArchivo);
    EstamosEnElHorno h;

    h.tempInterna( e.tempInterna() );
    cout << "Temperatura interna: " << e.tempInterna() << endl;
    h.tempExterna( e.tempExterna() );
    //cout << "Temperatura externa: " << e.tempExterna() << endl;
    h.radioExterno( e.radioExterno() );
    cout << "Radio externo: " << e.radioExterno() << endl;
    h.radioInterno( e.radioInterno() );
    cout << "Radio interno: " << e.radioInterno() << endl;
    h.cantMuestrasRadio( e.cantMuestrasRadio() );
    cout << "Muestras radios: " << e.cantMuestrasRadio() << endl;
    e.tempExterna().mostrar();
    cout << "Termino de leer" << endl;
    h.result();
    //h.generarInstancia(40,60,"data/prueba_1.dat");
    //h.generarPrueba_1(40,60);
    e.close();

    cout << "Fin de la ejecución" << endl;
	return 0;
}
