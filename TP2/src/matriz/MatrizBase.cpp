/*
 * MatrizBase.cpp
 *
 *  Created on: May 11, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "MatrizBase.h"

void MatrizBase::inicializar () {
	filas = 1;
	columnas = 1;
	miMatriz = new FLOTANTE * [1];
	miMatriz[0] = new FLOTANTE [1];
	miMatriz[0][0] = (FLOTANTE) 0;
}

/***
 * Constructor vac�o para cuando se necesita s�lo declarar una
 * variable sin a�n indicar sus dimensiones
 */
MatrizBase::MatrizBase () {
	inicializar();
}

MatrizBase::MatrizBase (unsigned int n, unsigned int m) {
	inicializar();
	redimensionar(n, m);

}

MatrizBase::MatrizBase (const MatrizBase& M) {
	inicializar();
	redimensionar(M.filas, M.columnas);

	// Copiar la matriz M en esta matriz
	for (unsigned int i = 0; i < this->columnas; i++) {
		for (unsigned int j = 0; j < this->filas; j++) {
			set(i,j, M.miMatriz[i][j]);
		}
	}
}

/***
 * Genera una matriz columna de N elementos
 */
MatrizBase::MatrizBase (unsigned int n) {
	inicializar();
    redimensionar(n, 1);
}

MatrizBase::~MatrizBase() {

}

void MatrizBase::mostrar() {
	for (unsigned int i = 0; i < filas; i++){
		for (unsigned int j = 0; j < columnas; j++){
			printf("   %.5LF",get(i,j));
		}
		cout << "" << endl;
	}
};

const FLOTANTE MatrizBase::get(unsigned int i, unsigned int j) {
	return miMatriz[i][j];
};

/***
 * Devuelve el elemento 'i' teniendo en cuenta que se trata de
 * una matriz columna
 */
const FLOTANTE MatrizBase::get(unsigned int i) {
	return miMatriz[i][0];
};

bool MatrizBase::esParecidaA (MatrizBase &m, FLOTANTE error) {
	return false;
};

unsigned int MatrizBase::getFilas() {
	return filas;
};

unsigned int MatrizBase::getColumnas() {
	return columnas;
};

void MatrizBase::sumar(unsigned int i, unsigned int j, FLOTANTE x) {};


void MatrizBase::redimensionar(unsigned int n) {
	redimensionar(n,1);
}

/***
 * Genera una nueva matriz, copiando el contenido de la matriz ya
 * existente en la nueva , dejando fuera de la matriz los n�meros que
 * queden fuera (si es una matriz menor) o rellenano con ceros (si es
 * una matriz mayor)
 */
void MatrizBase::redimensionar(unsigned int f, unsigned int col) {
	if (f == 0 || col == 0) {
		cout << "CONSTRUCTOR error n:" << f << " m:" << col << endl;
		cout << "No se redimensiona la matriz" << endl;
		return;
	}
	unsigned int filaMenor = (f < filas) ? f : filas;
	unsigned int colMenor = (col < columnas) ? col : columnas;
	FLOTANTE **nuevaMatriz = new FLOTANTE * [f];

	for (unsigned int i = 0; i < filaMenor; i++){
		nuevaMatriz[i] = new FLOTANTE [col];
		// Copia los elementos de la matriz anterior
		for (unsigned int j = 0; j < colMenor; j++) {
			nuevaMatriz[i][j] = miMatriz[i][j];
		}

		// Si quedan elementos sin copiar (la matriz nueva es mas grande)
		// entonces los inicializa con cero
		for (unsigned int j = colMenor; j < col; j++) {
			nuevaMatriz[i][j] = 0;
		}
	}

	// Si quedan elementos sin copiar (la matriz nueva es mas grande)
	// entonces los inicializa con cero
	for (unsigned int i = filaMenor; i < f; i++) {
		nuevaMatriz[i] = new FLOTANTE [col];
		for (unsigned int j = 0; j < col; j++) {
			nuevaMatriz[i][j] = 0;
		}
	}

	liberar();
	miMatriz = nuevaMatriz;
	filas = f;
	columnas = col;
}

void MatrizBase::set(unsigned int i, unsigned int j, FLOTANTE x) {
	if ( (i >= filas) || (j >= columnas) ) {
		cout << "SET::ERROR. Se quiere asignar un elemento fuera de los limites" << endl;
		cout << "SET::ERROR. filas:" << filas << " columnas:" << columnas << " i:" << i << " j:" << j << endl;
		cout << "Valor no almacenado" << endl;
		return;
	}
	miMatriz[i][j] = x;
}


void MatrizBase::liberar () {
	if (miMatriz != NULL) {
		for (unsigned int count = 0; count < filas; count++){
			if (miMatriz[count] != NULL ) {
				delete 	miMatriz[count];
			}
		}
		delete miMatriz;
	}
	miMatriz = NULL;
}

/**
 * Coloca el valor X en el elemento 'i', suponiendo que la
 * matriz es una matriz columna.
 */
void MatrizBase::set(unsigned int i, FLOTANTE x) {
	set(i,0,x);
}

/**
 * Coloca el valor X en el elemento 'i', suponiendo que la
 * matriz es una matriz columna.
 */
void MatrizBase::set(unsigned int i, int x) {
	set(i,0, (FLOTANTE) x);
}


void MatrizBase::set(unsigned int i, unsigned int j, int x) {
	set(i,j, (FLOTANTE) x);
}

void generarArchivoMatLab(const char* nombreMatriz,const char* nombreArchivo,MatrizBase & matriz) {
	FILE * fp=fopen(nombreArchivo,"w+");
	fprintf(fp,"# name: %s\n# type: matrix\n# rows: %d\n# columns: %d\n",nombreMatriz,matriz.getFilas(),matriz.getColumnas());
	for(unsigned int i = 0; i < matriz.getFilas(); i++) {
		for(unsigned int j = 0; j < matriz.getColumnas(); j++) {
			fprintf(fp," %.10LF",matriz.get(i,j));
		}
		fprintf(fp,"\n");
	}
	fclose(fp);
}

void generarTerminoIndependienteMatLab(const char* nombreMatriz,const char* nombreArchivo,MatrizBase & matriz) {
	FILE * fp=fopen(nombreArchivo,"w+");
	fprintf(fp,"# name: %s\n# type: matrix\n# rows: %d\n# columns: %d\n",nombreMatriz,matriz.getFilas(),1);
	for(unsigned int i = 0; i < matriz.getFilas(); i++) {
		fprintf(fp," %.10LF",matriz.get(i,matriz.getFilas()));
		fprintf(fp,"\n");
	}
	fclose(fp);
}
