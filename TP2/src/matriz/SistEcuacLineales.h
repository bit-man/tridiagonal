/*
 * SistEcuacLineales.h
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef SISTECUACLINEALES_H_
#define SISTECUACLINEALES_H_

#include "MatrizBase.h"

class SistEcuacLineales {

public:
	SistEcuacLineales() {};
	virtual void resolverSistema(MatrizBase & ayb) =0;
    virtual MatrizBase &result() =0;
	virtual ~SistEcuacLineales() {};
};

#endif /* SISTECUACLINEALES_H_ */
