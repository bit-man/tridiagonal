/*
 * MatrizFullFull.cpp
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */


#include "MatrizFullFull.h"


using namespace std;


/***
 * Constructor vac�o para cuando se necesita s�lo declarar una
 * variable sin a�n indicar sus dimensiones
 */
MatrizFullFull::MatrizFullFull(): MatrizBase::MatrizBase() {
}

MatrizFullFull::MatrizFullFull (unsigned int n, unsigned int m) :
	MatrizBase::MatrizBase(n,m) {
}

MatrizFullFull::MatrizFullFull (const MatrizFullFull& M) :
	MatrizBase::MatrizBase(M) {
}

MatrizFullFull::MatrizFullFull (unsigned int n) :
	MatrizBase::MatrizBase(n) {
}

void MatrizFullFull::redimensionar(unsigned int n) {
	MatrizBase::redimensionar(n);
}

void MatrizFullFull::redimensionar(unsigned int n, unsigned int m) {
	MatrizBase::redimensionar(n,m);
}


void MatrizFullFull::set(unsigned int i, unsigned int j, FLOTANTE x) {
	MatrizBase::set(i,j,x);
}

const FLOTANTE MatrizFullFull::get(unsigned int i, unsigned int j) {
	return MatrizBase::get(i,j);
}

const FLOTANTE MatrizFullFull::get(unsigned int i) {
	return MatrizBase::get(i);
}

void MatrizFullFull::set(unsigned int i, unsigned int j, int x) {
	MatrizBase::set(i,j,  x);
}

/**
 * Coloca el valor X en el elemento 'i', suponiendo que la
 * matriz es una matriz columna.
 */
void MatrizFullFull::set(unsigned int i, FLOTANTE x) {
	MatrizBase::set(i,x);
}

void MatrizFullFull::set(unsigned int i, int x) {
	MatrizBase::set(i,x);
}

void MatrizFullFull::mostrar() {
	MatrizBase::mostrar();
}


void MatrizFullFull::sumar(unsigned int i, unsigned int j, FLOTANTE x) {
	FLOTANTE temp = this->get(i,j) + x;
	this->set(i, j, temp);
}

unsigned int MatrizFullFull::getFilas() {
	return MatrizBase::getFilas();
}

unsigned int MatrizFullFull::getColumnas() {
	return MatrizBase::getColumnas();
}

MatrizFullFull::~MatrizFullFull() {
	liberar();
}

MatrizFullFull &MatrizFullFull::operator= (MatrizFullFull& M) {
	redimensionar(M.filas, M.columnas);
	for (unsigned int i = 0; i < filas; i++) {
		for (unsigned int j = 0; j < columnas; j++) {
			set(i,j, M.get(i,j));
		}
	}

  return *this;
}

bool operator== (MatrizFullFull& a, MatrizFullFull& b) {
	return (a.filas == b.filas && a.columnas == b.columnas &&
			a.coincideCon(b) );
}

bool MatrizFullFull::coincideCon(MatrizFullFull& m) {
	return this->esParecidaA(m, (FLOTANTE) 0);
}


bool MatrizFullFull::esParecidaA (MatrizFullFull &m, FLOTANTE error) {
	bool sonParecidas = true;

	for( unsigned int i = 0; i < filas && sonParecidas; i++) {
		for( unsigned int j=0; j < columnas && sonParecidas; j++ ) {
			sonParecidas = fabs(this->get(i,j) - m.get(i,j)) <= error;
		}
	}

	return sonParecidas;
}
