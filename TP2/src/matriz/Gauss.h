/*
 * Gauss.h
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef GAUSS_H_
#define GAUSS_H_


#include "SistEcuacLineales.h"
#include "MatrizFullFull.h"

class Gauss : public SistEcuacLineales {
private:
    MatrizFullFull _result;
public:
	Gauss();
	void resolverSistema(MatrizBase & ayb);
	MatrizFullFull &result();
	virtual ~Gauss();
};

#endif /* GAUSS_H_ */
