/*
 * Gauss.cpp
 *
 *  Created on: May 5, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include "Gauss.h"

Gauss::Gauss() {
}

void Gauss::resolverSistema(MatrizBase & ayb) {

	//Libro "Analisis Numerico", R. Burden y J. Douglas Faires, 7 edicion espanol
	//ALGORTIMO 6.2, pag 362,

	unsigned int nRow[ayb.getFilas()];
	MatrizFullFull m(ayb.getFilas(),ayb.getColumnas());

	//Unico dejo de originalidad del algoritmo:
	//Verifico que la matriz sea de Nx(N+1)

	if ( (ayb.getFilas() + 1) != ayb.getColumnas()  ) {
		cout << "ERROR:resolverSistema. El sistema no es Nx(N+1) filas:" << ayb.getFilas() << " columnas:" << ayb.getColumnas();
	 	return;
	}


	//Paso 1. Inicializo el puntero a renglon
	for (unsigned int i = 0; i < ayb.getColumnas(); i++) {
		nRow[i]	= i;
	}


	//Paso 2. proceso de eliminacion
	for (unsigned int i = 0; i < (ayb.getColumnas() - 1); i++) {
		//Paso 3.
		//Sea p el entero mas peque�o tal q' |a(NROW(p),i)| = max i<=j<n |a(NROW(j),i)|
		//Es decir busco el elemento de la columnas i, mas grande en valor absoluto
		//entre las filas que todavia no triangule

		//Inicializo convenientemente a "p"
		//para que si todos los elementos de la i-esima columna
		//dan 0, que en i
		int p = i;
		for (unsigned int count = i ; count < ayb.getFilas(); count++) {
			FLOTANTE valorParaFilaPColumnaI = ayb.get( nRow[p], i );
			FLOTANTE valorParaFilaCountColumnaI = ayb.get( nRow[count], i );

			if (fabs(valorParaFilaCountColumnaI) > fabs(valorParaFilaPColumnaI)){
				p = count;
			}
		}

		//Paso 4. verifica la existencia de soluci�n �nica
		if ( ayb.get( nRow[p], i) == 0 ) {
			cout << "Paso 4. No existe una solucion unica" << endl;
			return;
		}

		//Paso 5. Intercambio de filas simulado
		if ( nRow[i] != nRow[p] ) {
			unsigned int nCopy = nRow[i];
			nRow[i] = nRow[p];
			nRow[p] = nCopy;
		}

		//Paso 6.
		for (unsigned int j = i + 1; j< ayb.getFilas(); j++) {

			//Paso 7.
			FLOTANTE valor_j_i = ayb.get(nRow[j],i);
			FLOTANTE valor_i_i = ayb.get(nRow[i],i);
			m.set(nRow[j], i, valor_j_i / valor_i_i);

			//Paso 8. Hago las operaciones ente Filas E_(j) - m_(j,i) * E_(i)
			for (unsigned int colAux = i; colAux<ayb.getColumnas(); colAux++) {
				FLOTANTE valorAux =
					ayb.get(nRow[j], colAux) - m.get(nRow[j], i) * ayb.get(nRow[i], colAux);
				ayb.set(nRow[j], colAux, valorAux) ;
			}
		}

/*
 * DEPURACION
		cout << endl << "FINAL DEL PASO 2. i:" << i << endl;
		ayb.mostrar();
*/
	} //FIN PROCESO DE TRIANGULACION

	//Paso 9.
	if ( ayb.get( nRow[ayb.getFilas() - 1 ], ayb.getFilas() - 1) == 0 ) {
		cout << "Paso 9. No existe una solucion unica" << endl;
		return;
	}

	//Paso 10. Comienzo de la sustitucion hacia atras
    _result.redimensionar(ayb.getFilas(), 1);


	//x[n] = ...
	//NOTA: RECORDEMOS QUE:
	// N = ayb.getFilas() - 1
	// N + 1 = ayb.getFilas()
	_result.set(ayb.getFilas() - 1, 0, ayb.get( nRow[ayb.getFilas() - 1], ayb.getFilas()) / ayb.get( nRow[ayb.getFilas() - 1], ayb.getFilas()));

	//Paso 11. para i = n - 1, ... 1
	for (unsigned int i = ayb.getFilas() - 1; i < ayb.getFilas(); i--) {
		FLOTANTE sumatoria = 0;
		//realizo la sumatoria del paso 11
		for ( unsigned int j = i + 1; j < ayb.getFilas(); j++){
			sumatoria += ayb.get(nRow[i], j) * _result.get(j,0);
		}

		FLOTANTE valor_i_nMas1 = ayb.get(nRow[i], ayb.getFilas());
		FLOTANTE valor_i_i = ayb.get(nRow[i], i);

		FLOTANTE aux = (valor_i_nMas1 - sumatoria) / valor_i_i;
		_result.set(i, 0, aux);
	}

	//Paso 12. Salida. Procedimiento terminado exitosamente

}

MatrizFullFull &Gauss::result(){
	return _result;
};
Gauss::~Gauss() {
}
