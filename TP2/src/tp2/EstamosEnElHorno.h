/*
 * Gauss.h
 *
 *  Created on: May 10, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */


#ifndef ESTAMOS_EN_EL_HORNO_H_
#define ESTAMOS_EN_EL_HORNO_H_

#include "../matriz/MatrizBase.h"
#include "../matriz/MatrizFullFull.h"
#include "../matriz/Gauss.h"
#include<vector>

using namespace std;

#define PI   (FLOTANTE) 3.141592653

/***
 * Esta clase nos permite tomar los datos del problema y convertirlos en una
 * matriz para entregarlos a un método de triangulación
 *
 * Los datos que son necesario para la resolución del problema son :
 * - Temperatura interna : ingresada a través de tempInterna()
 * - Temperaturas externas tomadas con los sensores : ingresada a través de
 *        tempExterna()
 * - Radio interno : ingresado a través de radioInterno()
 * - Radio externo : ingresado a través de radioExterno()
 * - delta Radio : ingresado a través de deltaRadio() o calculado si se ingresa
 *        la cantidad de muestras con cantMuestrasRadio()
 * - delta Theta : calculado cuando se ingresan las temperaturas externas
 *        tomadas con los sensores usando tempExterna();
 ***/

class EstamosEnElHorno {
    private:
    	static const float TEMPERATURA_INTERNA = 1500;
    	static const float RADIO_INTERNO = 0.5;
    	static const float RADIO_EXTERNO = 1;
    	static const int TEMPERATURA_MAXIMA = 200;
    	static const int TEMPERATURA_MINIMA = 20;
        MatrizBase _a;
        FLOTANTE _tempInt;
        MatrizBase _tempExt;
        FLOTANTE _deltaTheta;
        FLOTANTE _deltaR;
        unsigned int _nRadio;  // Cantidad de muestras (radio)
        FLOTANTE _radioExt;
        FLOTANTE _radioInt;
        FLOTANTE  _centro(unsigned int j, unsigned int k);
        FLOTANTE  _izquierda(unsigned int j, unsigned int k);
        FLOTANTE  _derecha(unsigned int j, unsigned int k);
        FLOTANTE  _arriba(unsigned int j, unsigned int k);
        FLOTANTE  _abajo(unsigned int j, unsigned int k);
        FLOTANTE _terminoIndependiente(unsigned int j, unsigned int k);
        unsigned int _columna(unsigned int j, unsigned int k);
        unsigned int _fila(unsigned int j, unsigned int k);
        bool _fase1Ejecutada;
        bool _fase2Ejecutada;
        MatrizFullFull _result;
        MatrizFullFull _temperaturas;
    public:
    	EstamosEnElHorno();
    	~EstamosEnElHorno();
        void tempInterna( FLOTANTE t );
        void tempExterna( MatrizBase & t );
        void radioExterno( FLOTANTE r );
        void radioInterno( FLOTANTE r );
        void deltaRadio( FLOTANTE dr );
        void cantMuestrasRadio( unsigned int n );
        void result();
        void calcularTemperaturas();
        void _fase1();
        void _fase2();
        void generarMatrizX();
        void generarMatrizY();
        void generarMatrizTemperaturas();
        int numeroAleatorioEnRango(int piso, int techo);
        void generarPrueba_1(int cantRadios,int temperaturas);
        void generarPrueba_2(int cantRadios,int temperaturas);
		void generarPrueba_3(int cantRadios,int temperaturas);
		void generarPrueba_4(int cantRadios,int temperaturas);
};

typedef struct {
	FLOTANTE radio;
	FLOTANTE angulo;
} Punto;

void generarAngulosTermica(vector<Punto> & termica);
void generarRadiosTermica(vector<Punto> & termica);

#endif /* ESTAMOS_EN_EL_HORNO_H_ */
