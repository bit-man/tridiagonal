/*
 * EstamosEnElHorno.h
 *
 *  Created on: May 10, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#include "EstamosEnElHorno.h"
#include<vector>
#include <cstdlib>
#include <time.h>
#include<fstream>

using namespace std;


void EstamosEnElHorno::tempInterna( FLOTANTE t) {
    _tempInt = t;
};

void EstamosEnElHorno::tempExterna( MatrizBase & t){
    _tempExt = t;
    _deltaTheta = 2 * PI / t.getFilas();
};

void EstamosEnElHorno::deltaRadio( FLOTANTE dr ){
    _deltaR = dr;
};


void EstamosEnElHorno::radioExterno( FLOTANTE r ) {
    _radioExt = r;
};

void EstamosEnElHorno::radioInterno( FLOTANTE r ) {
    _radioInt = r;
};

void EstamosEnElHorno::cantMuestrasRadio( unsigned int n ) {
    _nRadio = n;
    _deltaR = ( _radioExt - _radioInt ) / n;
    cout << "delta R: " << _deltaR << endl;
};


void EstamosEnElHorno::result() {
	_fase1();
	_fase2();
}

void EstamosEnElHorno::_fase1() {
	if (_fase1Ejecutada) {
		return;
	}

	// Cada uno de los puntos de temperatura es una incóginta, pot lo tanto
	// tenemos #muestras_de_discretización_del_radio * #sensores incógintas
	unsigned int dim = (_nRadio - 1) * _tempExt.getFilas();
	_a.redimensionar(dim,dim + 1); // 1 fila más para la matriz (columna) de coef. independientes

	unsigned int fil,col;

	for(unsigned int k = 0; k < _tempExt.getFilas(); k++) {
		for(unsigned int j = 2; j < _nRadio - 1; j++) {
			//Centro
			fil = _fila(j,k);
			col = _columna(j,k);
			_a.set(fil,col,_centro(j,k));
			//izquierda
			col = _columna(j,k-1);
			_a.set(fil,col,_izquierda(j,k));
			//derecha
			col = _columna(j,k+1);
			_a.set(fil,col,_derecha(j,k));
			//arriba
			col = _columna(j-1,k);
			_a.set(fil,col,_arriba(j,k));
			//abajo
			col = _columna(j+1,k);
			_a.set(fil,col,_abajo(j,k));
			//Termino independiente
			_a.set(fil,dim,0);
		}
	}

	for(unsigned int k = 0; k < _tempExt.getFilas(); k++) {
		unsigned int j = 1;
		//Centro
		fil = _fila(j,k);
		col = _columna(1,k);
		_a.set(fil,col,_centro(j,k));
		//izquierda
		col = _columna(j,k-1);
		_a.set(fil,col,_izquierda(j,k));
		//derecha
		col = _columna(j,k+1);
		_a.set(fil,col,_derecha(j,k));
		//Termino independiente
		_a.set(fil,dim,-(_arriba(j,k))*_tempExt.get(k));
		//abajo
		col = _columna(j+1,k);
		_a.set(fil,col,_abajo(j,k));
	}

	for(unsigned int k = 0; k < _tempExt.getFilas(); k++) {
		unsigned int j = _nRadio - 1;
		//Centro
		fil = _fila(j,k);
		col = _columna(j,k);
		_a.set(fil,col,_centro(j,k));
		//izquierda
		col = _columna(j,k-1);
		_a.set(fil,col,_izquierda(j,k));
		//derecha
		col = _columna(j,k+1);
		_a.set(fil,col,_derecha(j,k));
		//Arriba
		col = _columna(j-1,k);
		_a.set(fil,col,_arriba(j,k));
		//Termino independiente
		_a.set(fil,dim,-(_abajo(j,k))*_tempInt);
	}

	_fase1Ejecutada = true;
}


void EstamosEnElHorno::_fase2() {

	vector<Punto> termica;
	vector<Punto> puntos;

	if (_fase2Ejecutada) {
		return;
	}
	Gauss gauss;
	gauss.resolverSistema(_a);
	MatrizFullFull & temp = gauss.result();
	unsigned int filas,columnas;
	filas = _nRadio+1;
	columnas = _tempExt.getFilas();
	_temperaturas.redimensionar(filas,columnas);
	for(unsigned int i = 0; i < columnas; i++) {
		_temperaturas.set(0,i,_tempExt.get(i));
		_temperaturas.set(filas-1,i,_tempInt);
	}
	for(unsigned int j = 0; j < columnas; j++) {
		for(unsigned int i = 1; i < filas-1; i++) {
			_temperaturas.set(i,j,temp.get(j*(filas-2) + i-1));
		}
	}
	//Interpolo por las funciones dadas por los radios
	for(unsigned int i = 0; i < columnas; i++) {
		int inicio = 0;
		int fin = filas-1;
		while(true) {
			if(_temperaturas.get(inicio,i) > 500) break;
			int medio = inicio + (fin - inicio)/2;
			if(_temperaturas.get(medio,i) >= 500) {
				fin = medio;
			} else {
				inicio = medio;
			}
			if(fin - inicio  <= 1) {
				Punto punto;
				punto.angulo = inicio;
				punto.radio = fin;
				puntos.push_back(punto);
				break;
			}
		}
	}

	for(unsigned int i = 0; i < puntos.size(); i++) {
		int inicio = puntos[i].angulo;
		int fin = puntos[i].radio;
		FLOTANTE xi = _radioExt - inicio*_deltaR;
		FLOTANTE xf = _radioExt - fin*_deltaR;
		FLOTANTE yi = _temperaturas.get(inicio,i);
		FLOTANTE yf = _temperaturas.get(fin,i);
		FLOTANTE radio = (500 - yi)*((xf-xi)/(yf-yi)) + xi;
		FLOTANTE angulo = _deltaTheta*i;
		Punto punto;
		punto.angulo = angulo;
		punto.radio = radio;
		termica.push_back(punto);
		int inicio_sig = puntos[(i+1)%puntos.size()].angulo;
		int desde, hasta;
		if(inicio > inicio_sig) {
			desde = inicio_sig+1;
			hasta = inicio;
			for(int j = hasta; j >= desde; j--) {
				xi = i*_deltaTheta;
				xf = (i+1)*_deltaTheta;
				yi = _temperaturas.get(j,i);
				yf = _temperaturas.get(j,(i+1)%puntos.size());
				FLOTANTE angulo = (500 - yi)*((xf-xi)/(yf-yi)) + xi;
				FLOTANTE radio = _radioExt - _deltaR*j;
				punto.angulo = angulo;
				punto.radio = radio;
				termica.push_back(punto);
			}
		} else if(inicio_sig > inicio) {
			desde = inicio+1;
			hasta = inicio_sig;
			for(int j = desde; j <= hasta; j++) {
				xi = i*_deltaTheta;
				xf = (i+1)*_deltaTheta;
				yi = _temperaturas.get(j,i);
				yf = _temperaturas.get(j,(i+1)%puntos.size());
				FLOTANTE angulo = (500 - yi)*((xf-xi)/(yf-yi)) + xi;
				FLOTANTE radio = _radioExt - _deltaR*j;
				punto.angulo = angulo;
				punto.radio = radio;
				termica.push_back(punto);
			}
		}
	}
	//Si todas las temperaturas externas son mayores que 500 no existe la termica
	if(termica.size() > 0) {
		Punto punto;
		punto.angulo = 2*PI;
		punto.radio = termica[0].radio;
		termica.push_back(punto);
	}

	generarAngulosTermica(termica);
	generarRadiosTermica(termica);
	generarMatrizY();
	generarMatrizX();
	generarMatrizTemperaturas();
};

void EstamosEnElHorno::calcularTemperaturas() {

};

FLOTANTE  EstamosEnElHorno::_centro(unsigned int j, unsigned int k) {
	FLOTANTE rj = _radioExt - j*_deltaR;
	FLOTANTE centro = ((FLOTANTE)1 / (rj*_deltaR)) - ((FLOTANTE)2 / pow(_deltaR,2)) - ((FLOTANTE)2 / (pow(rj,2)*pow(_deltaTheta,2)));
	return centro;
}

FLOTANTE  EstamosEnElHorno::_izquierda(unsigned int j, unsigned int k) {
	FLOTANTE rj = _radioExt - j*_deltaR;
	FLOTANTE izquierda = ((FLOTANTE)1 / (pow(rj,2)*pow(_deltaTheta,2)));
	return izquierda;
}

FLOTANTE  EstamosEnElHorno::_derecha(unsigned int j, unsigned int k) {
	FLOTANTE rj = _radioExt - j*_deltaR;
	FLOTANTE derecha = ((FLOTANTE)1 / (pow(rj,2)*pow(_deltaTheta,2)));
	return derecha;
}

FLOTANTE  EstamosEnElHorno::_arriba(unsigned int j, unsigned int k) {
	float rj = _radioExt - j*_deltaR;
	float arriba = ((FLOTANTE)1 / pow(_deltaR,2)) - ((FLOTANTE)1 / (rj*_deltaR));
	return arriba;
}
FLOTANTE  EstamosEnElHorno::_abajo(unsigned int j, unsigned int k) {
	FLOTANTE abajo = ((FLOTANTE)1 / pow(_deltaR,2));;
	return abajo;
}

FLOTANTE EstamosEnElHorno::_terminoIndependiente(unsigned int j, unsigned int k) {
	if(j == 1) {
		return -(_tempExt.get(k,0))*_arriba(j,k);
	} else if (j == (_nRadio - 2)) {
		return -(_tempInt)*_abajo(j,k);
	} else {
		return 0;
	}
}

/***
 * Calcula la columna a la cual convertir los valores de fila y columna pasados
 */
unsigned int EstamosEnElHorno::_columna(unsigned int j, unsigned int k) {
	// Básicamente divide el espacio en franjas del tamaño de la cant. de
	// discreatizaciones del radio y luego cada columna está representada dentro de
	// esta franja como un elemento adjacente a la columna anterior y posterior a jColumna
	if(k == (unsigned int)-1) {
		k = _tempExt.getFilas()-1;
	}
	if(k == _tempExt.getFilas()) {
		k = 0;
	}
	return k * (_nRadio - 1) + j - 1;
}

/***
 * Calcula la fila a la cual convertir los valores de fila y columna pasados.
 */
unsigned int EstamosEnElHorno::_fila(unsigned int j, unsigned int k) {
	return k * (_nRadio - 1) + j - 1;
}


EstamosEnElHorno::EstamosEnElHorno() {
	_fase1Ejecutada = false;
	_fase2Ejecutada = false;
};

EstamosEnElHorno::~EstamosEnElHorno() {};

void generarAngulosTermica(vector<Punto> & termica) {
	FILE * fp=fopen("angulos.m","w+");
	fprintf(fp,"a = [");
	for(unsigned int i = 0; i < termica.size(); i++) {
		fprintf(fp," %.10LF",termica[i].angulo);
	}
	fprintf(fp,"];");
	fclose(fp);
}

void generarRadiosTermica(vector<Punto> & termica) {
	FILE * fp=fopen("radios.m","w+");
	fprintf(fp,"r = [");
	for(unsigned int i = 0; i < termica.size(); i++) {
		fprintf(fp," %.10LF",termica[i].radio);
	}
	fprintf(fp,"];");
	fclose(fp);
}

void EstamosEnElHorno::generarMatrizX() {
	FILE * fp=fopen("matriz_x.m","w+");
	fprintf(fp,"X = [");
	FLOTANTE x,radio,angulo;
	for(unsigned int i = 0; i < _nRadio+1; i++) {
		radio = _radioExt - i*_deltaR;
		for(unsigned int j = 0; j < _tempExt.getFilas()+1; j++) {
			angulo = _deltaTheta*j;
			x = radio*cos(angulo);
			fprintf(fp," %.10LF",x);
		}
		if(i < _nRadio)fprintf(fp,";");
	}
	fprintf(fp,"];");
}

void EstamosEnElHorno::generarMatrizY() {
	FILE * fp=fopen("matriz_y.m","w+");
	fprintf(fp,"Y = [");
	FLOTANTE y,radio,angulo;
	for(unsigned int i = 0; i < _nRadio+1; i++) {
		radio = _radioExt - i*_deltaR;
		for(unsigned int j = 0; j < _tempExt.getFilas()+1; j++) {
			angulo = _deltaTheta*j;
			y = radio*sin(angulo);
			fprintf(fp," %.10LF",y);
		}
		if(i < _nRadio)fprintf(fp,";");
	}
	fprintf(fp,"];");
}

void EstamosEnElHorno::generarMatrizTemperaturas() {
	FILE * fp=fopen("matriz_temp.m","w+");
	fprintf(fp,"Z = [");
	int columnas = _temperaturas.getColumnas();
	for(unsigned int i = 0; i < _temperaturas.getFilas(); i++) {
		for(int j = 0; j < columnas+1; j++) {
			if(j < columnas) {
				fprintf(fp," %.10LF",_temperaturas.get(i,j));
			} else {
				fprintf(fp," %.10LF",_temperaturas.get(i,0));
			}
		}
		if(i < _temperaturas.getFilas()-1)fprintf(fp,";");
	}
	fprintf(fp,"];");
}

int EstamosEnElHorno::numeroAleatorioEnRango(int piso, int techo) {
	srand(rand() + time(NULL));
	int aleatorio = piso + (rand() % ((techo + 1) - piso));
	return aleatorio;
}

void EstamosEnElHorno::generarPrueba_1(int cantRadios,int temperaturas) {
	ofstream out;
	out.open("data/prueba_1.dat");
	out << TEMPERATURA_INTERNA << " " << RADIO_INTERNO << " " << RADIO_EXTERNO << " " << cantRadios << endl;
	for(int i = 0; i < temperaturas; i++) {
		out << numeroAleatorioEnRango(TEMPERATURA_MINIMA,TEMPERATURA_MAXIMA) << " ";
	}
	out << endl;
	out.close();
}

void EstamosEnElHorno::generarPrueba_2(int cantRadios,int temperaturas) {
	ofstream out;
	out.open("data/prueba_2.dat");
	out << TEMPERATURA_INTERNA << " " << RADIO_INTERNO << " " << RADIO_EXTERNO << " " << cantRadios << endl;
	FLOTANTE paso = (FLOTANTE)180/(temperaturas-1);
	for(int i = 0; i < temperaturas; i++) {
		out << 20 + paso*i << " ";
	}
	out << endl;
	out.close();
}

void EstamosEnElHorno::generarPrueba_3(int cantRadios,int temperaturas) {
	ofstream out;
	out.open("data/prueba_3.dat");
	out << TEMPERATURA_INTERNA << " " << RADIO_INTERNO << " " << RADIO_EXTERNO << " " << cantRadios << endl;
	for(int i = 0; i < temperaturas; i++) {
		if(i < temperaturas/2) {
			out << 20 << " ";
		} else {
			out << 200 << " ";
		}
	}
	out << endl;
	out.close();
}

void EstamosEnElHorno::generarPrueba_4(int cantRadios,int temperaturas) {
	ofstream out;
	out.open("data/prueba_4.dat");
	out << TEMPERATURA_INTERNA << " " << RADIO_INTERNO << " " << RADIO_EXTERNO << " " << cantRadios << endl;
	for(int i = 0; i < temperaturas; i++) {
		if(i % 2 == 0) {
			out << 20 << " ";
		} else {
			out << 200 << " ";
		}
	}
	out << endl;
	out.close();
}
