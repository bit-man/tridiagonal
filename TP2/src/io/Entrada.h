/*
 * Entrada.h
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Martín Lafont, Víctor A. Rodríguez
 */

#ifndef ENTRADA_H_
#define ENTRADA_H_

#include <iostream>
#include "EntradaCSV.h"
#include "../matriz/MatrizFullFull.h"

using namespace std;

class Entrada : public EntradaCSV {
private:
	FLOTANTE _tempInterna;
	FLOTANTE _radioInterno;
	FLOTANTE _radioExterno;
	unsigned int _cantMuestrasRadio;
	MatrizFullFull _matriz;
	bool _tempInternaLeida;
	bool _radioInternoLeido;
	bool _radioExternoLeido;
	bool _cantMuestrasRadioLeidas;
	bool _matrizLeida;

	void _leerMatriz();
	void _leerDatoReal( FLOTANTE & datum, bool & flag );
	void _leerDatoEntero(unsigned int & datum, bool & leido);
public:
	Entrada(const char *nombre): EntradaCSV(nombre) {
		_tempInternaLeida = false;
		_radioInternoLeido = false;
		_radioExternoLeido = false;
		_cantMuestrasRadioLeidas = false;
		_matrizLeida = false;
	};
    FLOTANTE tempInterna();
    FLOTANTE radioInterno();
    FLOTANTE radioExterno();
	unsigned int cantMuestrasRadio();
	MatrizFullFull& tempExterna();
	virtual ~Entrada();
};

#endif /* ENTRADA_H_ */
