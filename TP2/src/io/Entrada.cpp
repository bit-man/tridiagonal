/*
 * Entrada.cpp
 *
 *  Created on: May 3, 2009
 *      Author: Claudio Gauna, Víctor A. Rodríguez
 */

#include "Entrada.h"


Entrada::~Entrada() {
}

void Entrada::_leerMatriz() {;
	unsigned int dimension = 1;

	while( hayMasDatosEnEstaLinea() ) {
	    _matriz.redimensionar(dimension);
	    FLOTANTE n = leerReal();
		_matriz.set(dimension - 1, n);
		dimension++;
	}
}

FLOTANTE Entrada::tempInterna() {
	_leerDatoReal(_tempInterna, _tempInternaLeida);
    return _tempInterna;
};


FLOTANTE Entrada::radioInterno() {
    tempInterna();
	_leerDatoReal(_radioInterno, _radioInternoLeido);
    return _radioInterno;
};


FLOTANTE Entrada::radioExterno(){
    tempInterna();
    radioInterno();
	_leerDatoReal(_radioExterno, _radioExternoLeido);
    return _radioExterno;
};


unsigned int Entrada::cantMuestrasRadio(){
    tempInterna();
    radioInterno();
    radioExterno();
    bool habiaSidoLeido = _cantMuestrasRadioLeidas;
	_leerDatoEntero(_cantMuestrasRadio, _cantMuestrasRadioLeidas);
	if ( ! habiaSidoLeido) {
		siguienteLinea();
	}
    return _cantMuestrasRadio;
};



MatrizFullFull & Entrada::tempExterna() {
    tempInterna();
    radioInterno();
    radioExterno();
    cantMuestrasRadio();
    if (! _matrizLeida) {
		_leerMatriz();
		siguienteLinea();
		_matrizLeida = true;
	}

    return _matriz;
}


void Entrada::_leerDatoReal(FLOTANTE & datum, bool & leido) {
	if (! leido) {
		datum = leerReal();
		leido = true;
	}
}

void Entrada::_leerDatoEntero(unsigned int & datum, bool & leido) {
	if (! leido) {
		datum = leerEntero();
		leido = true;
	}
}
