##----------- Simple
#set style fill solid 1.0
## plot sin(x) 

## --------- Lindo con polar
#set polar
#plot t*sin(t)

##----------- Estadisticas
##pop(x) = 103*exp((1965-x)/10)
##plot [1960:1990] 'population.dat', pop(x)

## ---------- Formato polar
##  angulo  radio  ... me falta la temperatura y asociarla a un color
#set polar
#set style fill solid 1.0
#set boxwidth 5.0 absolute
#plot 'polar.dat' with boxxy, 'polar2.dat' with boxxy



## pointtype : tipo de punto (redondos, cuadrados, etc.). 
##             El pointtype 5 son cuadrados
## pointsize : tamaño del punto
set pointsize 3
plot 0.5 * sin(x) title "1000 C" with points pointtype 5 , \
     0.75 * cos(x) title "2000 C" with points pointtype 5

## Si pudiera cambiar el set style fill solid X donde 0.0 es para 1500 grados
## y 1.0 es para 50 grados tego todo en una sola gama. Tambi�n el style
## deber�a poder incluirlo en el .DAT