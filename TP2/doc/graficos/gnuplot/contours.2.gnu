# Extraido de http://gnuplot.sourceforge.net/demo_4.0/contours.2.gnu
# set terminal png transparent nocrop enhanced font arial 8 size 420,320 
# set output 'contours.2.png'
set samples 20, 20
set isosamples 21, 21
set contour base
set cntrparam levels auto 20
set title "3D gnuplot demo - contour plot (more contours)" 0.000000,0.000000  font ""
set xlabel "X axis" -5.000000,-2.000000  font ""
set ylabel "Y axis" 4.000000,-1.000000  font ""
set zlabel "Z axis" 0.000000,0.000000  font ""
splot x*y
