set polar
set style fill solid 1.0
set pointsize 3
plot '1500.dat' title "1500 C" with points pointtype 7, \
     '1450.dat' title "1450 C" with points pointtype 7, \
     '1400.dat' title "1400 C" with points pointtype 7, \
     '1350.dat' title "1350 C" with points pointtype 7, \
     '1300.dat' title "1300 C" with points pointtype 7, \
     '1250.dat' title "1250 C" with points pointtype 7, \
     '1200.dat' title "1200 C" with points pointtype 7, \
     '1150.dat' title "1150 C" with points pointtype 7, \
     '1100.dat' title "1100 C" with points pointtype 7, \
     '1050.dat' title "1050 C" with points pointtype 7, \
     '1000.dat' title "1000 C" with points pointtype 7, \
      '950.dat' title "950 C" with points pointtype 7, \
      '900.dat' title "900 C" with points pointtype 7, \
      '850.dat' title "850 C" with points pointtype 7, \
      '800.dat' title "800 C" with points pointtype 7, \
      '750.dat' title "750 C" with points pointtype 7, \
      '700.dat' title "700 C" with points pointtype 7, \
      '650.dat' title "650 C" with points pointtype 7, \
      '600.dat' title "600 C" with points pointtype 7, \
      '550.dat' title "550 C" with points pointtype 7, \
      '500.dat' title "500 C" with points pointtype 7, \
      '450.dat' title "450 C" with points pointtype 7, \
      '400.dat' title "400 C" with points pointtype 7, \
      '350.dat' title "350 C" with points pointtype 7, \
      '300.dat' title "300 C" with points pointtype 7, \
      '250.dat' title "250 C" with points pointtype 7, \
      '200.dat' title "200 C" with points pointtype 7, \
      '150.dat' title "150 C" with points pointtype 7, \
      '100.dat' title "100 C" with points pointtype 7, \
       '50.dat' title "50 C" with points pointtype 7
